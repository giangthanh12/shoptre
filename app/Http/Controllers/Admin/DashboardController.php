<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
class DashboardController extends Controller
{
    public  function dashboard() {
        $orders = Order::where('status', 1)->latest()->get();
        // $countOrderStatus = Order::query()->select("orders.status",DB::raw('COUNT(orders.id) as y'))->groupBy("status")->get();
        $orderStatus = Order::ORDER_STATUS;

        $all_statuses = [0,1,2,3,4,5];
        $dataStatuses = [];
        $countOrderStatuses = DB::table(DB::raw("(SELECT status, count(*) as count FROM orders GROUP BY status) as orders"))
                        ->rightJoin(DB::raw("(SELECT '" . implode("' as status union select '", $all_statuses) . "' as status) as all_statuses"), function ($join) {
                            $join->on('orders.status', '=', 'all_statuses.status');
                        })
                        ->groupBy('all_statuses.status')
                        ->selectRaw('all_statuses.status, COALESCE(sum(orders.count), 0) as count')
                        ->get();
        foreach($countOrderStatuses as $countOrderStatus) {
            $dataStatuses[] = ["y" => $countOrderStatus->count, "label"=>$orderStatus[$countOrderStatus->status], "color"=>Order::generateColor($countOrderStatus->status), "status"=>$countOrderStatus->status];
        }

        $countOrderStatus = Order::where("status", 1)->get()->count();
        $countOrderProcessing = Order::where("status", 2)->get()->count();
        $countOrderShipped = Order::where("status", 3)->get()->count();
        $countOrderDelivered = Order::where("status", 4)->get()->count();
        $dataStatusOrders = [];


        return view("admin.index", compact("orders","dataStatuses","countOrderProcessing","countOrderShipped","countOrderDelivered","orderStatus"));
    }
}
