<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function viewCategory(Request $request) {
        $categories = Category::latest()->get();
        return view('backend.category.viewCategory', compact('categories'));
    }
    public function storeCategory(Request $request) {
        $request->validate([
            'category_name_vn'=>'required',
            'category_icon'=>'required'
        ]);
        Category::insert([
            'category_name_vn'=>$request->category_name_vn,
            'category_slug_vn'=>Str::slug($request->category_name_vn),
            'category_icon'=>$request->category_icon,
            'created_at'=>Carbon::now()
        ]);
        $notification = array(
            'message'=>'Insert Category successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function deleteCategory($id) {
        Category::findOrFail($id)->delete();
        $notification = array(
            'message'=>'Delete Category successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function editCategory($id) {
        $category = Category::findOrFail($id);
        return view('backend.category.editCategory', compact('category' ));
    }
    public function updateCategory(Request $request, $id) {
        $request->validate([
            'category_name_vn'=>'required',
            'category_icon'=>'required'
        ]);
        Category::findOrFail($id)->update([
            'category_name_vn'=>$request->category_name_vn,
            'category_slug_vn'=>Str::slug($request->category_name_vn),
            'category_icon'=>$request->category_icon,
        ]);
        $notification = array(
            'message'=>'Update Category successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.category.view')->with($notification);
    }
}
