<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PHPUnit\Framework\Constraint\Count;


class CountryController extends Controller
{
    public function index(Request $request) {
        $countries = Country::latest()->get();
        return view('backend.country.index', compact('countries'));
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
        ]);
        Country::create($validator->validated());
        $notification = array(
            'message'=>'Insert Country successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function delete($id) {
        Country::findOrFail($id)->delete();
        $notification = array(
            'message'=>'Delete Category successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function edit($id) {
        $country = Country::findOrFail($id);
        return view('backend.country.edit', compact('country' ));
    }
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
        ]);
        Country::findOrFail($id)->update($validator->validated());
        $notification = array(
            'message'=>'Update Country successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.country.view')->with($notification);
    }
}
