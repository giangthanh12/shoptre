<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Country;
use App\Models\MultiImage;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Image;
use App\Http\Services\UploadService;
class ProductController extends Controller
{
    protected $uploadService;
    protected $product;
    public function __construct(UploadService  $uploadService, Product $product)
    {
        $this->uploadService = $uploadService;
        $this->product = $product;
    }

    public function addProduct() {
        $product_units = Product::UNIT;
        $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
        $countries = Country::all();
        $categories = Category::orderBy('category_name_vn', 'ASC')->get();
        return view('backend.product.add',
            ['categories'=>$categories,
             'brands'=>$brands,
             'countries'=>$countries,
             'product_units'=>$product_units
            ]);
    }
    public function getSubSubCategories(Request $request) {
        $subsubCategories  = SubSubCategory::where('subCategory_id', $request->subCategory_id)->orderBy('Subsubcategory_name_vn', 'ASC')->get();
        return json_encode($subsubCategories);
    }

    public function saveProduct(Request $request) {
        try {
            $product = Product::create([
                'category_id'=>$request->category_id,
                'product_name_vn'=>$request->product_name_vn,
                'product_slug_vn'=>$request->product_slug_vn,
                'product_code'=>uniqid("SHOP"),
                'product_qty'=>$request->product_qty,
                'capacity'=>$request->capacity,
                'product_unit'=>$request->product_unit,
                'country_id'=>$request->country_id,
                'selling_price'=>$request->selling_price,
                'discount_price'=>$request->discount_price ?? null,
                'short_descp_vn'=>$request->short_descp_vn,
                'long_descp_vn'=>$request->long_descp_vn,
                'hot_deals'=>$request->hot_deals,
                'price_show'=> !empty($request->discount_price) && $request->discount_price > 0 ? $request->discount_price : $request->selling_price,
            ]);
            if($request->file) {
                dd("ok");
                $fileUpload = $this->uploadService->upload($request, 'product');
                $this->product->saveAttactment($product, $fileUpload->getOriginalContent(),Attachment::TYPE_MAIN, false);
            }
            if($request->files) {
                $fileUploads = $this->uploadService->uploadMulti($request, 'product');
                foreach($fileUploads->getOriginalContent() as $fileUpload) {
                    $this->product->saveAttactment($product, $fileUpload,Attachment::TYPE_MULTI, false);
                }
            }
            goto end;
        }
        catch (Exception $e) {
            Log::error($e);
            if(env('APP_ENV') !== 'production') dd($e);
        }
        end:
        $notification = array(
            'message'=>'Adding Product successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.product.manage')->with($notification);
    }
    public function manageProduct() {
        $products = Product::with('mainImage')->latest()->get();
        return view('backend.product.view', compact('products'));
    }
    public function editProduct($id) {
        $countries = Country::all();
        $categories = Category::orderBy('category_name_vn', 'ASC')->get();
        $product = Product::with(['mainImage', "subImages"])->findOrFail($id);
        return view('backend.product.edit', compact( 'categories','product', 'countries'));
    }
    public function updateProduct(Request $request) {

        $id = $request->product_id;
        $product = Product::findOrFail($id);
        if($request->file('file')) {
            if(count($product->mainImage) > 0)
            unlink(public_path($product->mainImage[0]->url));
            $fileUpload = $this->uploadService->upload($request, 'product');
            $this->product->saveAttactment($product, $fileUpload->getOriginalContent(),Attachment::TYPE_MAIN, true);
        }
        if($request->file('files')) {
            if(count($product->subImages)) {
                foreach ($product->subImages as $sub) {
                    if(file_exists(public_path($sub->url))) {
                        unlink(public_path($sub->url));
                    }
                }
            }
            $fileUploads = $this->uploadService->uploadMulti($request, 'product');
            foreach($fileUploads->getOriginalContent() as $key => $fileUpload) {
                $this->product->saveAttactment($product, $fileUpload,Attachment::TYPE_MULTI, $key == 0 ? true : false);
            }
        }
        $product->update([
            'category_id'=>$request->category_id,
            'product_name_vn'=>$request->product_name_vn,
            'product_slug_vn'=>Str::slug($request->product_name_vn),
            'product_qty'=>$request->product_qty,
            'selling_price'=>$request->selling_price,
            'capacity'=>$request->capacity,
            'product_unit'=>$request->product_unit,
            'country_id'=>$request->country_id,
            'discount_price'=>$request->discount_price ?? null,
            'short_descp_vn'=>$request->short_descp_vn,
            'long_descp_vn'=>$request->long_descp_vn,
            'hot_deals'=>$request->hot_deals,
            'price_show'=> !empty($request->discount_price) && $request->discount_price > 0 ? $request->discount_price : $request->selling_price,
        ]);
        $notification = array(
            'message'=>'Updating Product successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.product.manage')->with($notification);
    }

    public function updateMultiImage(Request $request) {
        $images = $request->file('multiImage');
        if(!$images) return redirect()->back();
        foreach ( $images as $key=>$img) {
            $img_del = MultiImage::findOrFail($key)->photo_name;
            unlink($img_del);
            $image_gen = hexdec(uniqid()).$img->getClientOriginalName();
            Image::make($img)->resize(917,1000)->save('upload/product/sub_thumbnais/'.$image_gen);
            MultiImage::findOrFail($key)->update([
                'photo_name'=>'upload/product/main_thumbnai/'.$image_gen,
                'updated_at'=>Carbon::now(),
            ]);
        }
        $notification = array(
            'message'=>'Updating Multi images successfully',
            'type'=>'info'
        );
        return redirect()->route('Product.manage')->with($notification);
    }
    public function deleteMultiImage($id) {
        $img =  MultiImage::findOrFail($id);
        unlink($img->photo_name);
        $img->delete();
        $notification = array(
            'message'=>'Deleting Multi images successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);

    }
    public function activeProduct($id) {
        Product::findOrFail($id)->update(['status'=>1]);
        $notification = array(
            'message'=>'Update status product successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function inactiveProduct($id) {
        Product::findOrFail($id)->update(['status'=>0]);
        $notification = array(
            'message'=>'Update status product successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function deleteProduct($id) {
        $product = Product::findOrFail($id);
        $images = $product->images;
        foreach($images as $image) {
            unlink(public_path($image->url));
        }
        $product->delete();
        $product->images()->delete();
        $notification = array(
            'message'=>'Deleting product successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
}
