<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Services\UploadService;
use App\Models\Attachment;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {
    protected $uploadService;
    protected $admin;
    public function __construct(UploadService $uploadService, Admin $admin)
    {
        $this->uploadService = $uploadService;
        $this->admin = $admin;
    }
    public  function login(Request  $request) {
        if($request->isMethod("post")) {
            $validatedData = $request->validate([
                'email' => ['required', 'email'],
                'password' => ['required'],
            ]);
            $credentials = $request->only("email", "password");

            if (Auth::guard("admin")->attempt($credentials)) {
               return redirect()->route("admin.dashboard");
            }
            return redirect()->back()->with("error","Your password or email is incorrect");
        }
        return view("backend.auth.admin_login");
    }
    public function  logout(Request  $request) {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
    public function profile(Request $request) {
        if($request->isMethod("post")) {
            $admin = Auth::guard("admin")->user();
            if($request->file) {
                $imgOld = optional($admin->image)->url;
                if($imgOld) unlink($imgOld);
                $fileUpload = $this->uploadService->upload($request, 'admin_profile');
                $this->admin->saveAttactment($admin, $fileUpload->getOriginalContent(),true);
            }
            $admin->name= $request->name;
            $admin->phone= $request->phone;
            $admin->save();
            $notification = array(
                'message'=>'Update profile successfully',
                'type'=>'success',
            );
            return back()->with($notification);
        }
        $adminProfile = Auth::guard("admin")->user();
        return view('admin.admin_profile', compact('adminProfile'));
    }
    public function changePassAdmin(Request $request) {
        if($request->isMethod("post")) {
            $request->validate([
                'current_password'=>'required',
                'password'=>'required|confirmed'
            ]);

           $data = Auth::guard("admin")->user();
           $hashedPassword = $data->password;
           if(Hash::check($request->current_password, $hashedPassword)) {
               $data->password = Hash::make($request->password);
               $data->save();
               $notification = array(
                'message'=>'Mật khẩu cập nhập thành công',
                'type'=>'success',
            );
            return redirect()->back()->with($notification);
           }
           else {
               $notification = array(
                   'message'=>'Mật khẩu hiện tại không chính xác.',
                   'type'=>'error',
               );

               return redirect()->back()->with($notification);
           }
        }
        return view('admin.admin_changePassword');
    }
}
?>
