<?php

namespace App\Http\Controllers\Admin;

use App\Events\OrderChanged;
use App\Http\Controllers\Controller;
use App\Models\DetailOrder;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
class OrderController extends Controller
{
public function managePendingOrder() {
    $orders = Order::where('status', 1)->latest()->get();
    return view('backend.order.pendingOrder', compact('orders'));
}
public function updateConfirmedOrder($order_id) {

    Order::findOrFail($order_id)->update([
        'status'=>'confirmed',
        'confirmed_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('admin.order.managePendingOrder')->with($notification);
}
public function detailOrder($order_id) {
    $order = Order::findOrFail($order_id);
    $order_products = DetailOrder::where('order_id', $order_id)->orderBy('id', 'DESC')->get();
    $product_units = Product::UNIT;
    $order_status = Order::ORDER_STATUS;
    $payment_types = Order::PAYMENT_TYPES;
    return view('backend.order.detailOrder', compact('order','order_products', 'product_units', 'order_status', 'payment_types'));
}

public function updateProcessingOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>2,
        'confirmed_date'=>Carbon::now()
    ]);
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('admin.order.manageProcessingOrder')->with($notification);
}
public function manageProcessingOrder() {
    $order_status = Order::ORDER_STATUS;
    $orders = Order::where('status', 2)->latest()->get();
    return view('backend.order.processingOrder', compact('orders',"order_status"));
}

public function updateShippedOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>3,
        'shipped_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('admin.order.manageShippedOrder')->with($notification);
}

public function manageShippedOrder() {
    $orders = Order::where('status', 3)->latest()->get();
    return view('backend.order.shippedOrder', compact('orders'));
}
public function updateDeliveredOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>4,
        'delivered_date'=>Carbon::now()
    ]);
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('admin.order.manageDeliveredOrder')->with($notification);
}

public function manageDeliveredOrder() {
    $orders = Order::where('status', 4)->latest()->get();
    return view('backend.order.deliveredOrder', compact('orders'));
}
public function updateReturnOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>5,
        'cancel_date'=>Carbon::now()
    ]);
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('admin.order.manageReturnOrder')->with($notification);
}
public function managecancelOrder() {
    $orders = Order::where('status', 0)->latest()->get();
    return view('backend.order.cancelOrder', compact('orders'));
}
public function invoiceOrder($order_id) {
    $order = Order::findOrFail($order_id);
    $order_products = DetailOrder::with('product')->where('order_id', $order_id)->orderBy('id', 'DESC')->get();
    $payment_types = Order::PAYMENT_TYPES;
    $pdf = PDF::loadView('backend.order.invoiceOrder', compact('order','order_products', 'payment_types'));

    return $pdf->download('invoice'.$order_id.'.pdf');
}

public function searchAllOrder() {
    return view('backend.order.searchAllOrder');
}
public function searchDaySave(Request $request) {
    $date = Carbon::parse($request->order_date)->format('d F Y') ;

$orders = Order::where('order_date', $date)->orderBy('id', 'DESC')->get();
return view('backend.order.allOrder', compact('orders'));

}
public function searchMonthSave(Request $request) {


$orders = Order::where('order_month', $request->order_month)->where('order_year', $request->order_year)->orderBy('id', 'DESC')->get();
return view('backend.order.allOrder', compact('orders'));
}
public function searchYearSave(Request $request) {
    $orders = Order::where('order_year', $request->order_year)->orderBy('id', 'DESC')->get();
return view('backend.order.allOrder', compact('orders'));
}
public function manageReturnOrder() {
    $orders = Order::where('status', 5)->orderBy('order_date', 'DESC')->get();
    return view('backend.order.returnOrder', compact('orders'));
}
public function returnSuccess($id) {
    $order = Order::findOrFail($id);
    $order->update([
        'return_order'=>2,
        'status'=>'cancel'
    ]);
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->back()->with($notification);
}
public function updateCancelOrder($order_id) {
    $order = Order::findOrFail($order_id);
    $order->status = 0;
    $order->save();
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('admin.order.manageCancelOrder')->with($notification);
}
}
