<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PHPUnit\Framework\Constraint\Count;


class ContactController extends Controller
{
    public function index(Request $request) {
        $contacts = Contact::latest()->get();
        return view('backend.contact.index', compact('contacts'));
    }
}
