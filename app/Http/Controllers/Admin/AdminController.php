<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Pipeline;
use App\Actions\Fortify\AttemptToAuthenticate;
use Laravel\Fortify\Actions\EnsureLoginIsNotThrottled;
use Laravel\Fortify\Actions\PrepareAuthenticatedSession;
use App\Actions\Fortify\RedirectIfTwoFactorAuthenticatable;
use App\Http\Responses\LoginResponse;
use App\Http\Services\UploadService;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\LoginViewResponse;
use Laravel\Fortify\Contracts\LogoutResponse;
use Laravel\Fortify\Features;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Http\Requests\LoginRequest;
use Image;
class AdminController extends Controller
{
    protected $uploadService;
    /**
     * The guard implementation.
     *
     * @var \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $guard;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\StatefulGuard  $guard
     * @return void
     */

    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }
 public function manageAdmin() {
       $admins = Admin::whereNotIn("id",[Auth::guard("admin")->user()->id])->latest()->get();
       return view('backend.admin.view', compact('admins'));
   }
   public function addAdmin() {
       return view('backend.admin.add');
   }
    public function storeAdmin(Request $request) {
        $admin = new Admin();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->phone = $request->phone;
        $admin->password = Hash::make($request->password);
        $admin->type = 2;
        $admin->created_at = Carbon::now();
        $admin->save();
        if($request->file) {
            $fileUpload = $this->uploadService->upload($request, 'admin_profile');
            $admin->saveAttactment($admin, $fileUpload->getOriginalContent(),true);
        }
        $notification = array(
            'message'=>'Insert User successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.adminRole.manage')->with($notification);
    }
    public function editAdmin($id) {
        $admin = Admin::findOrFail($id);
        return view('backend.admin.edit', compact('admin'));
    }
    public function updateAdmin(Request $request, $id) {
        $admin = Admin::findOrFail($id);
        if($request->file) {
            $imgOld = optional($admin->image)->url;
            if($imgOld) unlink($imgOld);
            $fileUpload = $this->uploadService->upload($request, 'admin_profile');
            $admin->saveAttactment($admin, $fileUpload->getOriginalContent(),true);
        }


        Admin::findOrfail($id)->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'updated_at'=>Carbon::now()
        ]);
        $notification = array(
            'message'=>'Update User successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.adminRole.manage')->with($notification);
    }
    public function deleteAdmin($id) {
        $admin = Admin::findOrFail($id);
        $admin->delete();
        $notification = array(
            'message'=>'Delete User successfully',
            'type'=>'success'
        );
        return redirect()->route('admin.adminRole.manage')->with($notification);
    }

}

