<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\UploadService;
use App\Models\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Image;
use Illuminate\Support\Str;
use Mockery\Exception;

class SliderController extends Controller
{
    protected $uploadService;
    protected $slider;
    public  function __construct(Slider $slider,UploadService  $uploadService)
    {
        $this->uploadService = $uploadService;
        $this->slider = $slider;
    }

    public function manageSlider() {
        $sliders = Slider::with("image")->latest()->get();
        return view('backend.slider.manage', compact('sliders'));
    }
    public function storeSlider(Request $request) {
        $request->validate([
            'file'=>'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);
        DB::beginTransaction();
       try {
           $slider = Slider::create([
               'slider_title'=>$request->slider_title,
               'slider_description'=>$request->slider_description,
           ]);
           $fileUpload = $this->uploadService->upload($request, 'slider');
           $this->slider->saveAttactment($slider, $fileUpload->getOriginalContent(), false);
           DB::commit();
           $notification = array(
               'message'=>'Insert Slider successfully',
               'type'=>'success'
           );
           return redirect()->back()->with($notification);
       }
       catch (Exception $e) {
            DB::rollBack();
            Log::error($e);
       }
    }

    public function activeSlider($id) {
        Slider::findOrFail($id)->update(['status'=>Slider::ACTIVE]);
        $notification = array(
            'message'=>'Update status slider successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function inactiveSlider($id) {
        Slider::findOrFail($id)->update(['status'=>Slider::INACTIVE]);
        $notification = array(
            'message'=>'Update status slider successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function editSlider($id) {
        $slider = Slider::with("image")->findOrFail($id);
        return view('backend.slider.edit', compact('slider'));
    }
    public function updateSlider(Request $request, $id) {
        $slider = Slider::findOrFail($id);
        if($request->file('file')) {
            $request->validate([
                'file'=>'required|image|mimes:jpg,png,jpeg,gif,svg'
            ]);
            $imgOld = $slider->image->url;
            unlink($imgOld);
            $fileUpload = $this->uploadService->upload($request, 'slider');
            $this->slider->saveAttactment($slider, $fileUpload->getOriginalContent(), true);
        }
        $slider->update([
            'slider_title'=>$request->slider_title,
            'slider_description'=>$request->slider_description,
        ]);
        $notification = array(
            'message'=>'Update  slider successfully',
            'type'=>'info'
        );
        return redirect()->route('admin.slider.manage')->with($notification);
    }
    public function deleteSlider($id) {
        $slider = Slider::findOrFail($id);
        unlink($slider->image->url);
        $slider->image()->delete();
        $slider->delete();
        $notification = array(
            'message'=>'Deleting  slider successfully',
            'type'=>'info'
        );
        return redirect()->route('admin.slider.manage')->with($notification);
    }
}
