<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;

class HomeController extends Controller
{
 public  function  index() {
     $productsLatest = Product::where("status",ACTIVE) ->latest()->limit(12)->get();
     $productsHot = Product::where("status",ACTIVE)->where('hot_deals',ACTIVE)->latest()->limit(8)->get();
     return view("frontend_clone.frontend.index", compact('productsLatest', 'productsHot'));
 }
 public function about() {
    return view("frontend_clone.frontend.about");
 }
 public function contact(Request $request) {
    Contact::create($request->all());
    $dataSuccess = ["type"=>"success", "message"=>"Gửi thông tin thành công"];
    return back()->with($dataSuccess);
 }
}
