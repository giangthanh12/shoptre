<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Jobs\jobSendMail;
use App\Models\DetailOrder;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class CartController extends Controller
{
    public function index() {
        $dataCart = Cart::content();

        return view("frontend_clone.frontend.cart", compact("dataCart"));
    }
    public  function addToCart(Request $request) {
        $response = $this->checkProduct($request->productId, $request->qty, "add");
        if(!$response["result"]) return response()->json($response);
        $product = $response["data"];
        try {
            Cart::add([
                'id' => $product->id,
                'name' => $product->product_name_vn,
                'qty' => $request->qty,
                'price' => $product->price_show,
                'weight' => $product->capacity,
                'options' => [
                    'image'=>$product->mainImage[0]->url,
                    'unit' => $product->product_unit ]
            ]);
            return response()->json(["result"=>true, "data"=>[], "message" => "Thêm sản phẩm vào giỏ hàng thành công"]);
        }
        catch (Exception $e) {
            return response()->json(["result"=>false, "data"=>[], "message" => "Có lỗi xảy ra"]);
        }
    }
    public  function updateToCart(Request $request) {
        $response = $this->checkProduct($request->productId, $request->qty,"update");
        if(!$response["result"]) return response()->json($response);
        $product = $response["data"];
        try {
            Cart::update($request->rowId, $request->qty);
            return response()->json(["result"=>true, "data"=>[], "message" => "Cập nhật sản phẩm vào giỏ hàng thành công"]);
        }
        catch (Exception $e) {
            return response()->json(["result"=>false, "data"=>[], "message" => "Có lỗi xảy ra"]);
        }
    }

    public function deleteToCart(Request $request) {
        try {
            $dataCart = Cart::content();
            $dataCart = Cart::get($request->rowId);
            if($dataCart) {
                Cart::remove($request->rowId);
                return response()->json(["result"=>true, "data"=>[], "message" => "Xóa sản phẩm trong giỏ hàng thành công"]);
            }
        }
        catch (Exception $e) {
            return response()->json(["result"=>false, "data"=>[], "message" => "Có lỗi xảy ra"]);
        }
    }

    public function getTotalCart() {
        try {
            $totalCart = Cart::subtotal();
            return response()->json(["result"=>true, "data"=>"$totalCart", "message" => "Xóa sản phẩm trong giỏ hàng thành công"]);
        }
        catch (Exception $e) {
            return response()->json(["result"=>false, "data"=>[], "message" => "Có lỗi xảy ra"]);
        }
    }
    public function getSmallCart() {
        $data["dataCart"] = Cart::content();
        $data["countCart"] = Cart::count();
        $data["totalCart"] = Cart::subtotal();
        return response()->json(["result"=>true, "data"=>$data, "message" => ""]);

    }
    public function checkProduct($productId, $qty, $method) {
        $data = [];
        $dataCart = Cart::content();
        $product = Product::find($productId);
        if(!$product) {
            $data["result"] = false;
            $data["data"] = [];
            $data["message"] = "Không tồn tại sản phẩm";
            return $data;
        }
        $filtered = $dataCart->filter(function ($value, $key) use($productId) {
            return $value->id == $productId;
        });
        $checkQty = $product->product_qty - $qty;
        if(count($filtered->all()) > 0) {
            if($method == "add") {
                $checkQty -= $filtered->first()->qty;
            }
        };
        if($checkQty >= 0) {
            $data["result"] = true;
            $data["data"] = $product;
            $data["message"] = "Còn hàng";
            return $data;
        }
        else {
            $data["result"] = false;
            $data["data"] = [];
            $data["message"] = "Sản phẩm đã hết hàng, bạn vui lòng chọn sản phẩm khác";
            return $data;
        }
    }
    public  function checkout(Request  $request) {
        $dataCart = Cart::content();
        $countCart = Cart::count();
        $totalCart = Cart::subtotal();
        if($request->isMethod("post")) {
            $order = new Order();
            // chung
            try {
                $storeOrder = Order::create([
                    "name" => $request->name,
                    "email" => $request->email,
                    "post_code" => $request->post_code,
                    "phone" => $request->phone,
                    "amount"=>str_replace("." ,"", $totalCart),
                    "payment_method" => $request->payment_method,
                    "address" => $request->address,
                    "status" =>0
                ]);
                foreach( Cart::content() as $item) {
                    DetailOrder::insert([
                        'order_id'=>$storeOrder->id,
                        'product_id'=>$item->id,
                        'qty'=>$item->qty,
                        'price'=>$item->price,
                        'created_at'=>Carbon::now()
                    ]);
                }
                $order = $storeOrder;
                DB::commit();
            }
            catch (Exception $st) {
                DB::rollBack();
                dd($st);
                $dataError = ["type"=>"error", "message"=>"Đã có lỗi xảy ra trong quá trình thanh toán, vui lòng thử lại"];
                return back()->with($dataError);
            }
            if($request->payment_method == Order::CASH_ON_DELIVERY) {
                DB::beginTransaction();
                try {
                    $order->status = 1;
                    $order->order_number = "ORDER" . time();
                    $order->save();
                    $this->updateStockProduct();
                    DB::commit();
                    //thực hiện gửi email
                    $data['dataCart'] = Cart::content();
                    $data['total'] = $totalCart;
                    $data['order_number'] = $order->order_number;
                    $data['order_date'] = Carbon::parse($order->created_at)->format("d-m-Y");
                    $data['invoice_no'] = null;
                    $data['notes'] = $order->address;
                    $data['email'] = $order->email;
                    dispatch(new jobSendMail($data));
                    Cart::destroy();
                    $dataSuccess = ["type"=>"success", "message"=>"Thanh toán đơn hàng thành công"];
                    return redirect("/")->with($dataSuccess);
                }
                catch (Exception $st) {
                    DB::rollBack();
                    dd($st);
                    $dataError = ["type"=>"error", "message"=>"Đã có lỗi xảy ra trong quá trình thanh toán, vui lòng thử lại"];
                    return back()->with($dataError);
                }
            }
            else if($request->payment_method == Order::PAYMENT_QR_MOMO) {
                $endpoint = "https://test-payment.momo.vn/v2/gateway/api/create";
                $partnerCode = 'MOMOBKUN20180529';
                $accessKey = 'klm05TvNBzhg7h7j';
                $secretKey = 'at67qH6mk8w5Y1nAyMoYKMWACiEi2bsa';
                $orderInfo = "Thanh toán qua MoMo";
                $amount = str_replace(".", "", $totalCart);
                $orderId = time() ."";
                $redirectUrl = "http://shoptre.test/callback-momo";
                $ipnUrl = "https://webhook.site/b3088a6a-2d17-4f8d-a383-71389a6c600b";
                $extraData = "";
                $partnerCode = $partnerCode;
                $accessKey = $accessKey;
                $secretKey = $secretKey;
                $orderId = $orderId; // Mã đơn hàng
                $orderInfo = $orderInfo;
                $amount = $amount;
                $redirectUrl = $redirectUrl;
                $extraData = $extraData;
                $requestId = time() . "";
                $requestType = "captureWallet";
                // $extraData = ($_POST["extraData"] ? $_POST["extraData"] : "");
                //before sign HMAC SHA256 signature
                $rawHash = "accessKey=" . $accessKey . "&amount=" . $amount . "&extraData=" . $extraData . "&ipnUrl=" . $ipnUrl . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&partnerCode=" . $partnerCode . "&redirectUrl=" . $redirectUrl . "&requestId=" . $requestId . "&requestType=" . $requestType;

                $signature = hash_hmac("sha256", $rawHash, $secretKey);
                $data = array('partnerCode' => $partnerCode,
                    'partnerName' => "Test",
                    "storeId" => "MomoTestStore",
                    'requestId' => $requestId,
                    'amount' => $amount,
                    'orderId' => $orderId,
                    'orderInfo' => $orderInfo,
                    'redirectUrl' => $redirectUrl,
                    'ipnUrl' => $ipnUrl,
                    'lang' => 'vi',
                    'extraData' => $extraData,
                    'requestType' => $requestType,
                    'signature' => $signature);
                $result = $this->execPostRequest($endpoint, json_encode($data));
                $jsonResult = json_decode($result, true);  // decode json
                // save add info order
                $order->order_number = $orderId;
                $order->save();
                return redirect($jsonResult["payUrl"]);
            }
            else if($request->payment_method == Order::PAYMENT_ACCOUNT_MOMO) {
                $endpoint = "https://test-payment.momo.vn/v2/gateway/api/create";
                $partnerCode = 'MOMOBKUN20180529';
                $accessKey = 'klm05TvNBzhg7h7j';
                $secretKey = 'at67qH6mk8w5Y1nAyMoYKMWACiEi2bsa';
                $orderInfo = "Thanh toán qua MoMo";
                $orderId = time() . "";
                $redirectUrl = "http://shoptre.test/callback-momo";
                $ipnUrl = url('dashboard');
                $amount = str_replace(".", "", $totalCart);
                    $extraData = "";
                    $requestId = time() . "";
                    $requestType = "payWithATM";
                    $extraData = $extraData;
                    //before sign HMAC SHA256 signature
                    $rawHash = "accessKey=" . $accessKey . "&amount=" . $amount . "&extraData=" . $extraData . "&ipnUrl=" . $ipnUrl . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&partnerCode=" . $partnerCode . "&redirectUrl=" . $redirectUrl . "&requestId=" . $requestId . "&requestType=" . $requestType;
                    $signature = hash_hmac("sha256", $rawHash, $secretKey);
                    $data = array('partnerCode' => $partnerCode,
                        'partnerName' => "Test",
                        "storeId" => "MomoTestStore",
                        'requestId' => $requestId,
                        'amount' => $amount,
                        'orderId' => $orderId,
                        'orderInfo' => $orderInfo,
                        'redirectUrl' => $redirectUrl,
                        'ipnUrl' => $ipnUrl,
                        'lang' => 'vi',
                        'extraData' => $extraData,
                        'requestType' => $requestType,
                        'signature' => $signature);
                    $result = $this->execPostRequest($endpoint, json_encode($data));
                    $jsonResult = json_decode($result, true);  // decode json

                    // save add info order
                    $order->order_number = $orderId;
                    $order->save();


                    //Just a example, please check more in there
                    return redirect($jsonResult['payUrl']);
                        }
        }
        return view("frontend_clone.frontend.checkout", compact('dataCart', 'countCart', 'totalCart'));
    }
    public function callback(Request $request) {
        if($request->resultCode == 0) {
            $order = Order::where("order_number", $request->orderId)->first();
            $order->status = 1;
            $order->save();
            $this->updateStockProduct();
            //thực hiện gửi email
            $data['dataCart'] = Cart::content();
            $data['total'] = Cart::subtotal();
            $data['order_number'] = $order->order_number;
            $data['order_date'] = Carbon::parse($order->created_at)->format("d-m-Y");
            $data['invoice_no'] = null;
            $data['notes'] = $order->address;
            $data['email'] = $order->email;
            dispatch(new jobSendMail($data));
            Cart::destroy();
            $dataSuccess = ["type"=>"success", "message"=>"Thanh toán đơn hàng thành công"];
            return redirect("/")->with($dataSuccess);
        }
        else {
            $dataFailse = ["type"=>"error", "message"=>"Thanh toán đơn hàng thất bại"];
            return redirect("/")->with($dataFailse);
        }
    }

    public function execPostRequest($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        return $result;
    }
    public function updateStockProduct() {
        if(Cart::count() > 0) {
            foreach(Cart::content() as $item) {
                $product = Product::find($item->id);
                if($product) {
                    $product->product_qty = $product->product_qty - $item->qty;
                    $product->save();
                }
            }
        }
    }
}
