<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Route;

class ProductController extends Controller
{
    public function index(Request $request) {
        $params = request()->query();
        $idCategoryChecked = array_key_exists("category", $params) ? json_decode($params["category"]) : [];
        $priceSelected = array_key_exists("price", $params) ? $params["price"] : null;
        $minPrice =  array_key_exists("min-price", $params) ? $params["min-price"] : null;
        $maxPrice =  array_key_exists("max-price", $params) ? $params["max-price"] : null;
        $currentRoute =  $request->fullUrl();
        $products = Product::with("mainImage")
            ->where("status", ACTIVE)
            ->when($priceSelected && in_array($params["price"], ["asc", "desc"]), function ($q) use($params) {
                return $q->orderBy('price_show', $params["price"]);
            })
            ->when($idCategoryChecked && !empty($idCategoryChecked), function ($q) use($idCategoryChecked) {
                return $q->whereIn('category_id', $idCategoryChecked);
            })
            ->when($idCategoryChecked && !empty($idCategoryChecked), function ($q) use($idCategoryChecked) {
                return $q->whereIn('category_id', $idCategoryChecked);
            })
            ->when(!is_null($minPrice) && !is_null($maxPrice), function ($q) use($minPrice, $maxPrice) {
                return $q->whereBetween('price_show', [$minPrice, $maxPrice]);
            })
            ->paginate(12)->withQueryString();
        $categories = Category::withCount(['products'=> function($q){
            $q->where('status', ACTIVE);
        }])->get();
        return view("frontend_clone.frontend.product_list", compact("products", "categories", "idCategoryChecked", "priceSelected", "currentRoute"));
    }

    public function detail($slug) {
        $product = Product::where("product_slug_vn", $slug)->first();
        if(!$product) return abort("404");
        $productsRelated = Product::where("category_id", $product->category_id)->where("id","<>", $product->id)->get();
        return view("frontend_clone.frontend.product_detail", compact("product", "productsRelated"));
    }
}
