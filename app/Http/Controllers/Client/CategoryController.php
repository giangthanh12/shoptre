<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
   public  function index($slug, Request $request) {
       $params = request()->query();
       $category = Category::query()->where("category_slug_vn", $slug)->first();
       $currentRoute =  $request->fullUrl();
       $priceSelected = array_key_exists("price", $params) ? $params["price"] : null;
       $minPrice =  array_key_exists("min-price", $params) ? $params["min-price"] : null;
       $maxPrice =  array_key_exists("max-price", $params) ? $params["max-price"] : null;
       $products = $category->products()
           ->when(!is_null($minPrice) && !is_null($maxPrice), function ($q) use($minPrice, $maxPrice) {
               return $q->whereBetween('price_show', [$minPrice, $maxPrice]);
           })
           ->when($priceSelected && in_array($params["price"], ["asc", "desc"]), function ($q) use($params) {
               return $q->orderBy('price_show', $params["price"]);
           })
           ->paginate(12);
       return view("frontend_clone.frontend.category", compact("category", 'products' , 'minPrice', 'maxPrice', 'priceSelected', 'currentRoute'));
   }
}
