<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuth extends Middleware
{

    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard("admin")->check()) {
            return redirect("/admin/login");
        }
        return $next($request);
    }
}
