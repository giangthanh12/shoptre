<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class ProductSearch extends Component
{
    public $query;
    public $productsSearch;
    public $highLightIndex;
    public function  mount() {
        $this->resetValue();
    }
    public function resetValue() {
        $this->query = "";
        $this->productsSearch = [];
        $this->highLightIndex = 0;
    }
    public function incrementHighlight() {
        if($this->highLightIndex == count($this->productsSearch) - 1) {
            $this->highLightIndex = 0;
            return;
        }
        $this->highLightIndex++;
    }
    public  function decrementHighlight() {
        if($this->highLightIndex == 0) {
            $this->highLightIndex = count($this->productsSearch) - 1;
            return;
        }
        $this->highLightIndex--;
    }

    public function selectProductSearch() {
        $productSearch = $this->productsSearch[$this->highLightIndex] ?? null;
        if($productSearch) {
            redirect()->route("client.products.detail", $productSearch->product_slug_vn);
        }
    }

    public function updatedQuery() {
        $this->productsSearch = Product::where("product_name_vn", "like", '%'.$this->query.'%')->where("status",ACTIVE)->latest()->limit(5)->get();
    }

    public function render()
    {
        return view('livewire.product-search');
    }
}
