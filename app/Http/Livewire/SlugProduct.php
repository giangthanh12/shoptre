<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Cviebrock\EloquentSluggable\Services\SlugService;

class SlugProduct extends Component
{
    public $product_name_vn;
    public $product_slug_vn;
    public $product;
    public function mount($product) {
        $this->product_name_vn = $product->product_name_vn;
        $this->product_slug_vn = $product->product_slug_vn;
    }
    public function render()
    {
        return view('livewire.slug-product');
    }
    public function updatedProductNameVn()
    {
        $this->product_slug_vn = SlugService::createSlug(Product::class, 'product_slug_vn', $this->product_name_vn);
    }
}
