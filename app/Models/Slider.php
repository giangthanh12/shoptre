<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    protected $guarded = [];
    public  function images() {
        return $this->morphMany(Attachment::class, "imageable");
    }
    public function saveAttactment($model, $file, $clear = false) {
        if($clear == true) {
            $model->image()->delete();
        }

        Attachment::create([
            'imageable_type' => get_class($model),
            'imageable_id' => $model->id,
            'url' => $file['filepath'],
        ]);
    }
    public  function image() {
        return $this->morphOne(Attachment::class, "imageable");
    }
}
