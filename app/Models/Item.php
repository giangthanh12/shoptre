<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];
    protected $table = 'items';
    public function parts()
        {
            return $this->hasMany('App\Models\Part');
        }
}
