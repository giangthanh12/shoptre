<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    const ORDER_STATUS = [
        0=>"Thất bại",
        1=>"Đang chờ",
        2=>"Đang xử lý",
        3=>"Đang vận chuyển",
        4=>"Đã vận chuyển thành công",
        5=>"Hoàn trả"
    ];

    const PAYMENT_TYPES = [
        1 => "Thanh toán tại nhà",
        2 => "Thanh toán qua qr",
        3 => "Thanh toán qua thẻ"
    ];

    const CASH_ON_DELIVERY = 1;
    const PAYMENT_QR_MOMO = 2;
    const PAYMENT_ACCOUNT_MOMO = 3;
    public static function generateColor($index) {
        switch ($index) {
            case 0:
                return "#dc3545";
                break;
            case 1:
                return "#17a2b8";
                break;
            case 2:
                return "#20c997";
                break;
            case 3:
                return "#ffc107";
                break;
            case 4:
                return "#28a745";
                break;
            case 5:
                return "#6f42c1";
                break;
        }
    }
}
