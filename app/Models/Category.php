<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'category_name_vn',
        'slug_name_vn',
        'category_icon'
    ];
    public  function products() {
        return $this->hasMany(Product::class);
    }
}
