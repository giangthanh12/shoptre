<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    const TYPE_MAIN = 0;
    const TYPE_MULTI = 1;
    protected $fillable = [
        'url',
        'imageable_type',
        'imageable_id',
        'type'
    ];
    // relationship table ?
    public function attachable() {
        return $this->morphTo();
    }
}

?>
