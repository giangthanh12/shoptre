<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const UNIT = [
        1=>"ml",
        2=>"l",
        3=>"g",
        4=>"kg"
    ];
    use Sluggable;
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'product_slug_vn' => [
                'source' => 'product_name_vn'
            ]
        ];
    }
    protected $guarded = [];
    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function brand() {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
    public function user() {
        return $this->belongsToMany(Role::class, 'wishlists', 'product_id', 'user_id');
    }
    public  function images() {
        return $this->morphMany(Attachment::class,"imageable");
    }
    public function mainImage() {
        return $this->images()->where("type",Attachment::TYPE_MAIN);
    }
    public function country() {
        return $this->belongsTo(Country::class,"country_id","id");
    }
    public function subImages() {
        return $this->images()->where("type",Attachment::TYPE_MULTI);
    }
    public function saveAttactment($model, $file, $type, $clear = false) {
        if($clear == true) {
            switch ($type) {
                case Attachment::TYPE_MAIN:
                    $model->mainImage()->delete();
                    break;
                case Attachment::TYPE_MULTI:
                    $model->subImages()->delete();
                    break;
                default:
                    $model->images()->delete();
            }
        }
        Attachment::create([
            'imageable_type' => get_class($model),
            'imageable_id' => $model->id,
            'url' => $file['filepath'],
            'type' => $type
        ]);
    }
}
