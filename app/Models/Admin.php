<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $table = 'admins';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function saveAttactment($model, $file, $clear = false) {
        if($clear == true) {
            $model->image()->delete();
        }

        Attachment::create([
            'imageable_type' => get_class($model),
            'imageable_id' => $model->id,
            'url' => $file['filepath'],
        ]);
    }
    public  function image() {
        return $this->morphOne(Attachment::class, "imageable");
    }
}
