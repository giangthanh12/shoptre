@extends('admin.admin_master')
@section('admin')

@php

@endphp
<div class="container-full">
    <!-- Main content -->

    <section class="content">
        <div class="row">
            <div class="col-xl-3 col-6">
                <a href="/admin/order/delivered">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-primary-light rounded w-60 h-60">
                            <i class="text-primary mr-0 font-size-24 mdi mdi-account-multiple"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Đơn hàng thành công</p>
                            <h3 class="text-white mb-0 font-weight-500">
                                {{$countOrderDelivered}}
                            </h3>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-xl-3 col-6">
                <a href="/admin/order/shipped">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-warning-light rounded w-60 h-60">
                            <i class="text-warning mr-0 font-size-24 mdi mdi-car"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Đơn hàng đang vận chuyển</p>
                                <h3 class="text-white mb-0 font-weight-500">
                                    {{$countOrderShipped}}
                               </h3>
                        </div>
                    </div>
                </div>
                 </a>
            </div>
            <div class="col-xl-3 col-6">
                <a href="/admin/order/processing">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-info-light rounded w-60 h-60">
                            <i class="text-info mr-0 font-size-24 mdi mdi-sale"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Đơn hàng đang xử lý</p>
                            <h3 class="text-white mb-0 font-weight-500">
                                {{$countOrderProcessing}}
                            </h3>
                        </div>
                    </div>
                </div>
                </a>
            </div>
            <div class="col-xl-3 col-6">
                <a href="/admin/order/pending">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-danger-light rounded w-60 h-60">
                            <i class="text-danger mr-0 font-size-24 mdi mdi-phone-incoming"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Đơn hàng chờ</p>
                            <h3 class="text-white mb-0 font-weight-500">{{count($orders)}}</h3>
                        </div>
                    </div>
                </div>
                </a>
            </div>
            <div class="col-12" style="margin-bottom: 48px;">
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>
            <br>
            <div class="col-12">
                <div class="box">
                    <div class="box-header">
                        <h4 class="box-title align-items-start flex-column">
                            Đơn hàng đang chờ
                        </h4>
                    </div>

                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>OrderId</th>
                                        <th>Amount</th>
                                        <th>Payment</th>
                                        <th>Status</th>
                                        <th width="20%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $order)
                                 <tr>
                                     <td>{{Carbon\Carbon::parse($order->created_at)->format('D, d/m/Y')}}</td>
                                     <td>{{$order->order_number}}</td>
                                     <td>{{number_format($order->amount,0,'','.')}}</td>
                                     <td>
                                         @if ($order->payment_method == 1)
                                             <span class="badge badge-success" >Thanh toán tại nhà</span>
                                         @else
                                             <span class="badge badge-warning">Thanh toán online</span>
                                         @endif
                                     </td>
                                     <td>
                                         <span class="badge badge-warning">{{$order->status == 1 ?  "Đang chờ"  : ""}}</span>
                                     </td>

                                     <td>
                                         <a href="{{route('admin.order.detail',$order->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-eye"></i></a>
                                 </tr>
                                    @endforeach

                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <script>
    window.onload = function() {

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        backgroundColor: "#272e48",
        title:{
            text: ""
        },
        axisY: {
            title: "Orders",
            indexLabelFontColor: "red",
        },
        data: [{
            color: "red",
            type: "column",
            yValueFormatString: "#,##0.## Đơn",
            dataPoints: <?php echo json_encode($dataStatuses, JSON_NUMERIC_CHECK); ?>
        }]
    });
        for(var i = 0; i < chart.options.data.length; i++) {
          dataSeries = chart.options.data[i];
          for(var j = 0; j < dataSeries.dataPoints.length; j++){
            dataSeries.dataPoints[j].color = dataSeries.dataPoints[j].color;
          }
        }
    chart.render();

    }
    </script>
@endsection
