@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Main content -->
    <section class="content">

        <!-- Basic Forms -->
         <div class="box">
           <div class="box-header with-border">
             <h4 class="box-title">Edit profile</h4>

           </div>
           <!-- /.box-header -->
           <div class="box-body">
             <div class="row">
               <div class="col">
                   <form method="POST" action="{{route('adminProfile.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                       <div class="col-12">
                           <div class="row">
                               <div class="col-md-4">
                                   <div class="form-group">
                                        <h5>Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" class="form-control" value="{{$dataEdit->name}}"> </div>
                                    </div>
                               </div>
                               <div class="col-md-4">
                                   <div class="form-group">
                                        <h5>Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control" value="{{$dataEdit->email}}"required="" aria-invalid="false"> <div class="help-block"></div></div>
                                    </div>
                               </div>
                               <div class="col-md-4">
                                <div class="form-group">
                                     <h5>Phone <span class="text-danger">*</span></h5>
                                     <div class="controls">
                                         <input type="number" name="phone" class="form-control" value="{{$dataEdit->phone}}"required="" aria-invalid="false"> <div class="help-block"></div></div>
                                 </div>
                            </div>
                           </div>

                           <div class="row">
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Avatar <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" id="image" name="profile_photo_path" class="form-control" >
                                        </div>
                                    </div>
                               </div>
                               <div class="col-md-6">
                                    <img width="100" height="100" id="showImage" src="{{!empty($dataEdit->profile_photo_path) ? url($dataEdit->profile_photo_path) : url('upload/user3-128x128.jpg')}}" alt="">
                               </div>
                           </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-info" value="Update">
                        </div>
                       </div>





                   </form>

               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->

       </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    $(document).ready(function() {
        $('#image').change(function(e) {
           var reader = new FileReader(); // tạo một file đọc
           reader.onload = function(e) {
            // console.log(e.target.result);
            $('#showImage').attr('src', e.target.result);
           } // quá trình đọc kết thúc sẽ được gán giá trị kết quả cho src
           if (e.target.files[0]) {
                reader.readAsDataURL(e.target.files[0]);// chỉ ra đó là file đọc data url
            }
        })
    });
  </script>
@endsection

