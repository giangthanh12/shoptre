<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="{{asset('backend/images/favicon.ico')}}">

    <title>Sunny Admin - Dashboard</title>

	<!-- Vendors Style-->
	<link rel="stylesheet" href=" {{asset('backend/css/vendors_css.css')}}">

	<!-- Style-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
	<link rel="stylesheet" href=" {{asset('backend/css/style.css')}}">
	<link rel="stylesheet" href=" {{asset('backend/css/skin_color.css')}}">
    @livewireStyles
    <style>
        /* Center the loader */
        #loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 40px;
  height: 40px;
  margin: -76px 0 0 -76px;
  border: 5px solid #f3f3f3;
  border-radius: 50%;
  border-top: 5px solid #9034db;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 }
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom {
  from{ bottom:-100px; opacity:0 }
  to{ bottom:0; opacity:1 }
}
        </style>
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-primary fixed " >
    <div id="loader"></div>
<div class="wrapper">

  @include('admin.body.header')

  <!-- Left side column. contains the logo and sidebar -->
@include('admin.body.sidebar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  @yield('admin')
  </div>
  <!-- /.content-wrapper -->
@include('admin.body.footer')


  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
	<!-- Vendor JS -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<script src="{{asset('backend/js/vendors.min.js')}}"></script>
    <script src="{{asset('assets/icons/feather-icons/feather.min.js')}}"></script>
	{{-- <script src="{{asset('assets/vendor_components/easypiechart/dist/jquery.easypiechart.js')}}"></script> --}}
	<script src="{{asset('assets/vendor_components/apexcharts-bundle/irregular-data-series.js')}}"></script>
	<script src="{{asset('assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}}"></script>

	<!-- Sunny Admin App -->

    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script src="{{asset('assets/vendor_components/datatable/datatables.min.js')}}"></script>
	<script src="{{asset('backend/js/pages/data-table.js')}}"></script>
    <script src="{{asset('assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js')}}"></script><!-- Input tags -->
    <script src="https://cdn.tiny.cloud/1/m0xmwy96yr86j4vrrny1p1l9qh9vkw40f0896qcrbfrvigt3/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        var editor_config = {
          path_absolute : "http://shoptre.test/",
          selector: 'textarea',
          relative_urls: false,
          plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table directionality",
            "emoticons template paste textpattern"
          ],
          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          file_picker_callback : function(callback, value, meta) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
            if (meta.filetype == 'image') {
              cmsURL = cmsURL + "&type=Images";
            } else {
              cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.openUrl({
              url : cmsURL,
              title : 'Filemanager',
              width : x * 0.8,
              height : y * 0.8,
              resizable : "yes",
              close_previous : "no",
              onMessage: (api, message) => {
                callback(message.content);
              }
            });
          }
        };

        tinymce.init(editor_config);
      </script>
    <script src="{{asset('backend/js/template.js')}}"></script>
    {{-- <script src="{{asset('backend/js/pages/dashboard.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> --}}
    <script>
        @if (Session::has('message'))
        var message = "{{Session::get('message')}}";
        var type = "{{Session::get('type')}}";
        console.log(message);
        switch(type) {
            case 'success':
            toastr.success(message);
            break;
            case 'info':
            toastr.info(message);
            break;
            case 'error':
            toastr.error(message);
            break;
            case 'warning':
            toastr.warning(message);
            break;
        }
        @endif
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $(document).on('click', '.delete', function(e) {
            e.preventDefault();
             e.stopPropagation();
             var href = $(this).attr('href');
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = href;
            }


            })
            })
            $(document).on('click', '#confirmed', function(e) {
            e.preventDefault();
             e.stopPropagation();
             var href = $(this).attr('href');
            Swal.fire({
            title: 'Bạn chắc chắn',
            text: "Bạn cập nhật đơn hàng này?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok'
            }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = href;
            }


            })
            })
        })

    </script>

    <script>
        $(document).ready(function() {
            document.getElementById("loader").style.display = "none";
        })
    </script>
    @livewireScripts
</body>
</html>
