<header class="main-header">
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top pl-30">
      <!-- Sidebar toggle button-->
	  <div>
		  <ul class="nav">
			<li class="btn-group nav-item">
				<a href="#" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="push-menu" role="button">
					<i class="nav-link-icon mdi mdi-menu"></i>
			    </a>
			</li>
			<li class="btn-group nav-item">
				<a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="Full Screen">
					<i class="nav-link-icon mdi mdi-crop-free"></i>
			    </a>
			</li>


		  </ul>
	  </div>

      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav">
		  <!-- full Screen -->

		  <!-- Notifications -->
          <style>
              span#num_notification {
                    font-size: 9px;
                    position: absolute;
                    top: 1px;
                }
          </style>


	      <!-- User Account-->
          <li class="dropdown user user-menu">
			<a href="#" class="waves-effect waves-light rounded dropdown-toggle p-0" data-toggle="dropdown" title="User">
                @php
                    $user = Auth::guard('admin')->user();
                @endphp
                <span>{{$user->name}}</span>
                <img src="{{!empty( $user->profile_photo_path) ? asset( $user->profile_photo_path) : url('upload/user3-128x128.jpg')}}" alt="">
			</a>
			<ul class="dropdown-menu animated flipInX">
			  <li class="user-body">
				 <a class="dropdown-item" href="{{route('admin.profile')}}"><i class="ti-user text-muted mr-2"></i> Hồ sơ</a>
				 <a class="dropdown-item" href="{{route('admin.changePass')}}"><i class="ti-wallet text-muted mr-2"></i> Đổi mật khẩu</a>
				 <div class="dropdown-divider"></div>
                  <form method="POST" action="{{ route('admin.logout') }}">
                      @csrf
                      <div class="logout-admin d-flex align-items-center">
                          <i class="ti-lock text-muted mr-2 pl-1"></i>
                          <input class="dropdown-item" style="width: 100%;" type="submit" value="Đăng xuất" />
                      </div>

                  </form>

              </li>
			</ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>
