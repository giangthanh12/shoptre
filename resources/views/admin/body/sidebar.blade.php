@php
 $route = Route::current()->getName();
    $prefix = request()->route()->getPrefix();
    $user = Auth::guard('admin')->user();

@endphp

<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
			<div class="ulogo">
				 <a href="{{url('admin/dashboard')}}">
				  <!-- logo for regular state and mobile devices -->
					 <div class="d-flex align-items-center justify-content-center">
						  <img src="{{asset('backend/images/logo-dark.png')}}" alt="">
						  <h3>Shop Tre</h3>
					 </div>
				</a>
			</div>
        </div>
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">

		<li class="{{$route === 'dashboard' ? 'active' : ''}}">
          <a href="{{url('admin/dashboard')}}">
            <i data-feather="pie-chart"></i>
			<span>Dashboard</span>
          </a>
        </li>
{{--        <li class="treeview {{$prefix === '/brand' ? 'active' : ''}}" >--}}
{{--            <a href="#">--}}
{{--              <i data-feather="message-circle"></i>--}}
{{--              <span>Brand</span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'Brand.view' ? 'active' : ''}}"><a href="{{route('Brand.view')}}" ><i class="ti-more"></i>All brand</a></li>--}}
{{--            </ul>--}}
{{--          </li>--}}




        <li class="{$prefix === '/category' ? 'active' : ''}}" >
            <a href="{{route('admin.category.view')}}">
              <i data-feather="message-circle"></i>
              <span>Category</span>
            </a>
          </li>
          <li class="{$prefix === '/country' ? 'active' : ''}}" >
              <a href="{{route('admin.country.view')}}">
                  <i data-feather="message-circle"></i>
                  <span>Country</span>
              </a>
          </li>
          <li class="{$prefix === '/contact' ? 'active' : ''}}" >
            <a href="{{route('admin.contact.view')}}">
                <i data-feather="message-circle"></i>
                <span>Contact</span>
            </a>
        </li>
          <li class="treeview" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Product</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'admin.product.add' ? 'active' : ''}}"><a href="{{route('admin.product.add')}}" ><i class="ti-more"></i>Add product</a></li>
              <li class="{{$route == 'admin.product.manage' ? 'active' : ''}}"><a href="{{route('admin.product.manage')}}" ><i class="ti-more"></i>Manage product</a></li>
            </ul>
          </li>

          {{-- <li class="treeview" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Slider</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'admin.slider.manage' ? 'active' : ''}}"><a href="{{route('admin.slider.manage')}}" ><i class="ti-more"></i>Manage Slider</a></li>
            </ul>
          </li> --}}


{{--          @if ($user->coupon == 1)--}}
{{--          <li class="treeview {{$prefix === '/coupon' ? 'active' : ''}}" >--}}
{{--            <a href="#">--}}
{{--              <i data-feather="message-circle"></i>--}}
{{--              <span>Coupon</span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'Coupon.manage' ? 'active' : ''}}"><a href="{{route('Coupon.manage')}}" ><i class="ti-more"></i>Manage Coupon</a></li>--}}
{{--            </ul>--}}
{{--          </li>--}}
{{--          @endif--}}
{{--          @if ($user->postCategory == 1)--}}
{{--          <li class="treeview {{$prefix === '/postCategory' ? 'active' : ''}}" >--}}
{{--            <a href="#">--}}
{{--              <i data-feather="message-circle"></i>--}}
{{--              <span>Post Category</span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'PostCategory.manage' ? 'active' : ''}}"><a href="{{route('PostCategory.manage')}}" ><i class="ti-more"></i>Manage Post Category</a></li>--}}
{{--            </ul>--}}
{{--          </li>--}}
{{--          @endif--}}

{{--          @if ($user->post == 1)--}}
{{--             <li class="treeview {{$prefix === '/post' ? 'active' : ''}}" >--}}
{{--            <a href="#">--}}
{{--              <i data-feather="message-circle"></i>--}}
{{--              <span>Posts </span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'Post.manage' ? 'active' : ''}}"><a href="{{route('Post.manage')}}" ><i class="ti-more"></i>manage</a></li>--}}
{{--            </ul>--}}
{{--          </li>--}}
{{--          @endif--}}

{{--          @if ($user->setting == 1)--}}
{{--        <li class="treeview {{$prefix === '/setting' ? 'active' : ''}}" >--}}
{{--            <a href="#">--}}
{{--              <i data-feather="message-circle"></i>--}}
{{--              <span>Setting </span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'Site.manage' ? 'active' : ''}}"><a href="{{route('Site.manage')}}" ><i class="ti-more"></i>Manage Site</a></li>--}}
{{--            </ul>--}}
{{--          </li>--}}
{{--          @endif--}}

       @if ($user->type == 1)
       <li class="treeview {{$prefix === '/adminRole' ? 'active' : ''}}">
           <a href="#">
             <i data-feather="file"></i>
             <span>Admin</span>
             <span class="pull-right-container">
               <i class="fa fa-angle-right pull-right"></i>
             </span>
           </a>
           <ul class="treeview-menu">
             <li class="{{$route == 'adminRole.manage' ? 'active' : ''}}"><a href="{{route('admin.adminRole.manage')}}"><i class="ti-more"></i>Manage admin</a></li>
           </ul>
       </li>
       @endif

{{--        <li class="treeview {{$prefix === '/module' ? 'active' : ''}}">--}}
{{--            <a href="#">--}}
{{--              <i data-feather="file"></i>--}}
{{--              <span>Module</span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="active"><a href="{{route('Module.manage')}}"><i class="ti-more"></i>Danh sách module</a></li>--}}

{{--            </ul>--}}
{{--        </li>--}}
        <!-- <li class="treeview {{$prefix === '/admin' ? 'active' : ''}}">
            <a href="#">
              <i data-feather="file"></i>
              <span>Admin</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class='active'><a href=""><i class="ti-more"></i>Danh sách admin</a></li>

            </ul>
        </li> -->


        <li class="header nav-small-cap">User Interface</li>
       <li class="treeview {{$prefix === '/order' ? 'active' : ''}}" >
           <a href="#">
             <i data-feather="message-circle"></i>
             <span>Orders</span>
             <span class="pull-right-container">
               <i class="fa fa-angle-right pull-right"></i>
             </span>
           </a>
           <ul class="treeview-menu">
             <li class="{{$route == 'admin.order.managePendingOrder' ? 'active' : ''}}"><a href="{{route('admin.order.managePendingOrder')}}" ><i class="ti-more"></i>Manage Pending Order</a></li>
             <li class="{{$route == 'admin.order.manageProcessingOrder' ? 'active' : ''}}"><a href="{{route('admin.order.manageProcessingOrder')}}" ><i class="ti-more"></i>Manage Processing Order</a></li>
             <li class="{{$route == 'admin.order.manageShippedOrder' ? 'active' : ''}}"><a href="{{route('admin.order.manageShippedOrder')}}" ><i class="ti-more"></i>Manage Shipped Order</a></li>
             <li class="{{$route == 'admin.order.manageDeliveredOrder' ? 'active' : ''}}"><a href="{{route('admin.order.manageDeliveredOrder')}}" ><i class="ti-more"></i>Manage Delivered Order</a></li>
             <li class="{{$route == 'admin.order.manageReturnOrder' ? 'active' : ''}}"><a href="{{route('admin.order.manageReturnOrder')}}" ><i class="ti-more"></i>Manage Return Order</a></li>
             <li class="{{$route == 'admin.order.manageCancelOrder' ? 'active' : ''}}"><a href="{{route('admin.order.manageCancelOrder')}}" ><i class="ti-more"></i>Manage Cancel Order</a></li>
           </ul>
       </li>









{{--        @if ($user->Alluser == 1)--}}
{{--                <li class="treeview {{$prefix === '/Alluser' ? 'active' : ''}}">--}}
{{--                    <a href="#">--}}
{{--                        <i data-feather="file"></i>--}}
{{--                        <span>Users</span>--}}
{{--                        <span class="pull-right-container">--}}
{{--                            <i class="fa fa-angle-right pull-right"></i>--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li class="{{$route == 'Alluser.view' ? 'active' : ''}}"><a href="{{route('Alluser.view')}}"><i class="ti-more"></i>All users</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--        @endif--}}
{{--        @if ($user->review == 1)--}}
{{--            <li class="treeview {{$prefix === '/admin/review' ? 'active' : ''}}">--}}
{{--            <a href="#">--}}
{{--                <i data-feather="file"></i>--}}
{{--                <span>Reviews</span>--}}
{{--                <span class="pull-right-container">--}}
{{--                    <i class="fa fa-angle-right pull-right"></i>--}}
{{--                </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'Review.manage' ? 'active' : ''}}"><a href="{{route('Review.manage')}}"><i class="ti-more"></i>Manage reviews</a></li>--}}
{{--            </ul>--}}
{{--        </li>--}}
{{--        @endif--}}



{{--        @if ($user->AllOrder ==1)--}}
{{--            <li class="treeview {{$prefix === '/AllOrder' ? 'active' : ''}}">--}}
{{--            <a href="#">--}}
{{--              <i data-feather="file"></i>--}}
{{--              <span>Search Orders</span>--}}
{{--              <span class="pull-right-container">--}}
{{--                <i class="fa fa-angle-right pull-right"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--              <li class="{{$route == 'Allorder.search' ? 'active' : ''}}"><a href="{{route('Allorder.search')}}"><i class="ti-more"></i>Search orders</a></li>--}}
{{--            </ul>--}}
{{--        </li>--}}
{{--        @endif--}}

      </ul>
    </section>
  </aside>
