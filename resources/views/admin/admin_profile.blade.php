@extends('admin.admin_master')
@section('admin')
<style>
    .widget-user .widget-user-image > img {
    width: 90px !important;
    height: 90px!important;
    border: 3px solid #ffffff!important;
    object-fit: cover!important;
}
</style>
<div class="container-full">
    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="box box-widget widget-user">
					<!-- Add the bg color to the header using any of the bg-* classes -->
					<div class="widget-user-header bg-black" style=" center center;">
					  <h3 class="widget-user-username">Name: {{$adminProfile->name}}</h3>
					  <h6 class="widget-user-desc">Email: {{$adminProfile->email}}</h6>
					</div>
					<div class="widget-user-image">
					  <img class="rounded-circle" src="{{!empty(optional($adminProfile->image)->url) ? url($adminProfile->image->url) : url('upload/user3-128x128.jpg')}}" alt="User Avatar">
					</div>
					<div class="box-footer">
                        <form  method="POST" action="{{route('admin.profile')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                             <h5>Name<span class="text-danger">*</span></h5>
                                             <div class="controls">
                                                 <input type="text" name="name" class="form-control" value="{{$adminProfile->name}}"> </div>
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                             <h5>Email <span class="text-danger">*</span></h5>
                                             <div class="controls">
                                                 <input type="email" style="pointer-events: none;" name="email" class="form-control" value="{{$adminProfile->email}}"required="" aria-invalid="false"> <div class="help-block"></div></div>
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                     <div class="form-group">
                                          <h5>Phone <span class="text-danger">*</span></h5>
                                          <div class="controls">
                                              <input type="number" name="phone" class="form-control" value="{{$adminProfile->phone}}"required="" aria-invalid="false"> <div class="help-block"></div></div>
                                      </div>
                                 </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                         <div class="form-group">
                                             <h5>Avatar <span class="text-danger">*</span></h5>
                                             <div class="controls">
                                                 <input type="file" id="image" name="file" class="form-control" >
                                             </div>
                                         </div>
                                    </div>
                                </div>
                             <div class="form-group">
                                 <input type="submit" class="btn btn-rounded btn-info" value="Update">
                             </div>
                            </div>
					    </div>
                        </form>
				  </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
