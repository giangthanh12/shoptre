@extends("frontend_clone.layouts.master")
@section("content")
<div class="container">
    <div class="row justify-content-center">
        <div class="">
            <div class="">
                <h3>Quên mật khẩu</h3>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="">Email của bạn</label>

                            <div class="col-md-6">
                                <div class="auth-form__gr" style="width:23.5%">
                                    <input type="text"  id="email" name="email" class="auth-form__input @error('email') is-invalid @enderror" placeholder="Email của bạn" required autocomplete="email" autofocus>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


