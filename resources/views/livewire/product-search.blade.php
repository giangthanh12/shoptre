<div>
    <form action="" wire:submit.prevent="selectProductSearch" class="search-bar--m dis_flex algin_center">
        <input class="search-bar__input"
               wire:keydown.escape="resetValue"
               wire:keydown.tab="resetValue"
               wire:model="query"
               wire:keydown.arrow-up="decrementHighlight"
               wire:keydown.arrow-down="incrementHighlight"
               type="text" style="font-size:18px;"
               placeholder="Tìm kiếm sản phẩm tại đây">
    </form>
    @if(!empty($query))
        <div style="position: fixed; top:0; left:0; bottom: 0;
        right: 0;" wire:click="resetValue"></div>
    <ul class="list-products-search">
        @foreach($productsSearch as $key=>$proSearch)
        <li class="li-item-product-search {{$highLightIndex == $key ? "active" : ""}}">
            <a class="item-product-search" href="{{route("client.products.detail",$proSearch->product_slug_vn)}}">
                <img style="width: 50px;height: 50px; margin-left: 10px; border-radius: 5px;" src="{{asset($proSearch->mainImage[0]->url)}}" alt="">
                <div class="right-search">
                    <p style="color: #000; font-weight: bold;">{{$proSearch->product_name_vn}}</p>
                    <p class="product-search-price" style="color: #000;">
                        @if(!is_null($proSearch->discount_price) && $proSearch->discount_price > 0)
                            {{number_format($proSearch->discount_price,0,",",",")}}Đ
                        @else
                            {{number_format($proSearch->selling_price,0,",",",")}}Đ
                        @endif
                    </p>
                </div>
            </a>
        </li>
        @endforeach
    </ul>
        @endif
</div>
