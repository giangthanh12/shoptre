
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Product name<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="text" name="product_name_vn" wire:model="product_name_vn"  id="product_name_vn" class="form-control" required="">
                @error('product_name_vn')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Product slug<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="text" name="product_slug_vn" wire:model="product_slug_vn" id="product_slug_vn" class="form-control" required="">
                @error('product_slug_vn')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </div>
</div>
