@extends('frontend.main_layout')

@section('content')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{url('/')}}">Trang chủ</a></li>
				<li class='active'>Đăng nhập</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
			<div class="row">
				<!-- Sign-in -->
<div class="col-md-6 col-sm-6 sign-in">
	<h4 class="">Đăng nhập</h4>
	<p class="">Chào mừng bạn đến với shop chúng tôi.</p>

	<form class="register-form outer-top-xs" id="form-login"  method="POST" role="form" action="{{ isset($guard) ? url($guard.'/login') : route('login') }}">
            @csrf
		<div class="form-group">
		    <label class="info-title" for="email">Email<span>*</span></label>
		    <input type="email"  class="form-control unicase-form-control text-input" name="email"  id="email" >
		</div>
	  	<div class="form-group">
		    <label class="info-title" for="password">Password <span>*</span></label>
		    <input type="password" name="password" class="form-control unicase-form-control text-input" id="password" >
		</div>
		<div class="radio outer-xs">

                <div class="remember_password">
                    <input type="checkbox"  style="margin: 0 0 0;" name="remember" id="remember_me">
                    <label style="padding-left:0;" for="remember_me">Nhớ mật khẩu</label>
                </div>


		  	<a href="{{ route('password.request') }}" class="forgot-password pull-right">Quên mật khẩu ?</a>
		</div>
	  	<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Đăng nhập</button>
	</form>
</div>
		</div><!-- /.row -->
		</div><!-- /.sigin-in-->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
<style>
    label.error {
    color: #eb3434;
}
</style>

<script type="text/javascript">
    $("#form-login").validate({
        rules: {
            email: {
            required: true,
            email: true
            },
            password:{
                required: true,
                minlength:6,
            },
        },
        messages: {
            password: {
                required: 'Vui lòng điền mật khẩu của bạn',
                minlength: 'Mật khẩu yêu cầu tối thiểu 6 ký tự'
            },
            email: {
            required: "Vui lòng nhập email của bạn",
            email: "Email của bạn không đúng định dạng"
            }
        }
        });
</script>



@endsection
