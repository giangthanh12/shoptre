@extends('frontend.main_layout')
@section('title', 'Đăng ký')
@section('content')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="home.html">Trang chủ</a></li>
				<li class='active'>Đăng ký</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
			<div class="row">
				<!-- Sign-in -->
<!-- Sign-in -->

<!-- create a new account -->
<div class="col-md-6 col-sm-6 create-new-account">
	<h4 class="checkout-subtitle">Tạo tài khoản tại đây</h4>
	<p class="text title-tag-line">Chào mừng bạn đến với shop của chúng tôi.</p>
	<form id="form-register" class="register-form outer-top-xs" action="{{ route('register') }}" method="POST" role="form">
        @csrf
		<div class="form-group">
	    	<label class="info-title" for="email">Email<span>*</span></label>
	    	<input type="email" name="email"  class="form-control unicase-form-control text-input" id="email" >
            @error('email')
                <span class="text-danger"><strong>{{$message}}</strong></span>
            @enderror
	  	</div>
        <div class="form-group">
		    <label class="info-title" for="name">Tên của bạn <span>*</span></label>
		    <input type="text" name="name" class="form-control unicase-form-control text-input" id="name" >
            @error('name')
                <span class="text-danger"><strong>{{$message}}</strong></span>
            @enderror
		</div>
        <div class="form-group">
		    <label class="info-title" for="phone">Số điện thoại<span>*</span></label>
		    <input type="text" name="phone" class="form-control unicase-form-control text-input" id="phone" >
            @error('phone')
                <span class="text-danger"><strong>{{$message}}</strong></span>
            @enderror
		</div>
        <div class="form-group">
		    <label class="info-title" for="password">Mật khẩu <span>*</span></label>
		    <input type="password" name="password" class="form-control unicase-form-control text-input" id="password" >
            @error('password')
                <span class="text-danger"><strong>{{$message}}</strong></span>
            @enderror
		</div>
         <div class="form-group">
		    <label class="info-title" for="password_confirmation">Nhập lại mật khẩu<span>*</span></label>
		    <input type="password" name="password_confirmation" class="form-control unicase-form-control text-input" id="password_confirmation" >
            @error('password_confirmation')
                <span class="text-danger"><strong>{{$message}}</strong></span>
            @enderror
        </div>
	  	<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Đăng ký</button>
	</form>
</div>
<!-- create a new account -->			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
<style>
    label.error {
    color: #eb3434;
}
</style>

<script type="text/javascript">
    $("#form-register").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
            },
            phone: {
                required:true,
                number:true,
                minlength:true,
            },
            password:{
                required: true,
                minlength:6,
            },
            password_confirmation:{
                equalTo: "#password"

            },
        },
        messages: {
            password: {
                required: 'Vui lòng điền mật khẩu của bạn',
                minlength: 'Mật khẩu yêu cầu tối thiểu 6 ký tự'
            },
            password_confirmation:{
                equalTo: 'Mật khẩu của bạn không khớp',

            },
            email: {
                required: "Vui lòng nhập email của bạn",
                email: "Email của bạn không đúng định dạng"
            },
            name: {
                required: 'Vui lòng nhập tên của bạn',
            },
            phone: {
                required:'Vui lòng nhập điện thoại của bạn',
                number:'Yêu cầu bắt buộc số từ 0-9',
                minlength:'Tối thiểu 10 số',
            }
        }
        });
</script>
@endsection
