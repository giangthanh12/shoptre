@extends('frontend.main_layout')

@section('content')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="home.html">Home</a></li>
				<li class='active'>Forget password</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
			<div class="row">
				<!-- Sign-in -->
<div class="col-md-6 col-sm-6 sign-in">
	<h4 class="">Lấy lại mật khẩu</h4>
	{{-- <p class=""></p> --}}
    @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
          <span class="text-success"><strong>{{ session('status') }}</strong></span>
        </div>
    @endif
	<form class="register-form outer-top-xs"  method="POST" role="form" action="{{route('password.email')}}">
            @csrf
		<div class="form-group">
		    <label class="info-title" for="email">Email Address <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input" name="email"  id="email" >
		</div>
	  	<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Lấy lại mật khẩu</button>
	</form>
</div>
<!-- Sign-in -->

<!-- create a new account -->

<!-- create a new account -->			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
@endsection
