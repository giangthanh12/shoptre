@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Admins</h3>
                 <a class="btn btn-primary float-right" href="{{route('admin.adminRole.add')}}">Thêm thành viên</a>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Image</th>
                               <th>Name</th>
                               <th>Email</th>
                               <th>Phone</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($admins as $admin)
                        <tr>
                            <td><img src="{{!empty(optional($admin->image)->url) ? url($admin->image->url) : url('upload/user3-128x128.jpg')}}" width="50" height="50" alt=""></td>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <th>+{{$admin->phone}}</th>
                            <td>
                                <a href="{{route('admin.adminRole.edit',$admin->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('admin.adminRole.delete',$admin->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>

                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

