@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Brands</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Slider image</th>
                               <th>Title</th>
                               <th>Description</th>
                               <th>Status</th>
                               <th width="30%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @forelse ($sliders as $slider)
                        <tr>
                            <td><img width="70" height="40" src="{{asset($slider->image->url)}}" alt=""></td>
                            <td>{{$slider->slider_title}}</td>
                            <td>{{$slider->slider_description}}</td>
                            <td>
                                @if ($slider->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-warning">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('admin.slider.edit',$slider->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('admin.slider.delete',$slider->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                                @if ($slider->status == 1)
                                <a href="{{route('admin.slider.inactive', $slider->id)}}" class="btn btn-danger mb-5" title="inactive"><i class="fa fa-arrow-down"></i></a>
                                @else
                                <a href="{{route('admin.slider.active', $slider->id)}}" class="btn btn-success mb-5" title="active"><i class="fa fa-arrow-up"></i></a>
                                @endif
                            </td>
                        </tr>
                           @empty
                           @endforelse

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add Slider</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('admin.slider.store')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Slider title <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="slider_title" value="{{old('slider_description')}}" id="slider_title" class="form-control">
                                    @error('slider_title')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Slider description<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="slider_description" id="slider_description" value="{{old('slider_description')}}" class="form-control">
                                    @error('slider_description')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Slider image <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="file" name="file" id="file" class="form-control">
                                    @error('slider_img')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Slider">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

