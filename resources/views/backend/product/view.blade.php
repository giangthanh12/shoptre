@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Brands</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Product code</th>
                               <th>Product thumbnail</th>
                               <th>Product name vn</th>
                               <th>Product price</th>
                               <th>Quantity</th>
                               <th>Discount</th>
                               <th>Status</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($products as $product)
                        <tr>
                            <td><strong>{{$product->product_code}}</strong></td>
                            <td><img src="{{asset(count($product->mainImage) ? $product->mainImage[0]->url : "")}}" width="50" height="50" alt=""></td>
                            <td>{{$product->product_name_vn}}</td>
                            <td>{{number_format($product->selling_price,0,'','.')}} Đ</td>
                            <th>{{$product->product_qty}}</th>
                            @if ($product->discount_price === 'Null')
                            <td><span class="badge badge-pill badge-danger">Null</span></td>
                            @else

                                @if (!empty($product->discount_price) && $product->discount_price > 0)
                                    @php
                                        $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100;
                                    @endphp
                                    <td><span class="badge badge-pill badge-success">{{round($discount)}} %</span></td>
                                @else
                                    <td><span class="badge badge-pill badge-warning">No discount</span></td>
                                @endif

                            @endif

                            <td>
                                @if ($product->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-warning">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('admin.product.edit',$product->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('admin.product.delete',$product->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                                @if ($product->status == 1)
                                    <a href="{{route('admin.product.inactive', $product->id)}}" class="btn btn-danger mb-5" title="inactive"><i class="fa fa-arrow-down"></i></a>
                                @else
                                <a href="{{route('admin.product.active', $product->id)}}" class="btn btn-success mb-5" title="active"><i class="fa fa-arrow-up"></i></a>
                                @endif
                            </td>
                        </tr>
                           @endforeach
                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

