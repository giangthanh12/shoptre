@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">


    <!-- Main content -->
    <section class="content">

     <!-- Basic Forms -->
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">Edit product: <span style="color:deepskyblue">{{$product->product_code}}</span></h4>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
                <form method="POST"  action="{{route('admin.product.update')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                  <div class="row">
                    <div class="col-12">
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Select category <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="category_id" id="category_id" class="form-control choose" required="">
                                        <option  selected="" disabled>Select category</option>
                                        @foreach ($categories as $category)
                                          <option {{$product->category_id === $category->id ? 'selected' : ''}} value="{{$category->id}}">{{$category->category_name_vn}}</option>
                                        @endforeach
                                      </select>
                                      @error('category_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Selling price<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text"  name="selling_price" value="{{number_format($product->selling_price,0,"","")}}" id="selling_price" required="" class="form-control" >
                                        @error('selling_price')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Product quantity <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_qty" required="" value="{{$product->product_qty}}" id="product_qty" class="form-control" >
                                        @error('product_qty')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Product Origin <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select name="country_id" id="country_id" class="form-control choose" required="">
                                            <option  selected="" disabled>Select origin</option>
                                            @foreach ($countries as $country)
                                                <option {{$product->country_id === $country->id ? 'selected' : ''}} value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('country_id')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire("slug-product",['product' => $product])
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Dung tích<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input min="100" max="2000" type="text" name="capacity" value="{{old("capacity", $product->capacity)}}"  id="capacity" class="form-control" />
                                        @error('capacity')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Unit<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select name="product_unit" id="product_unit" class="form-control choose" required="">
                                            <option  selected="" disabled>Select unit</option>
                                            @foreach(\App\Models\Product::UNIT as $key=>$unit)
                                            <option value="{{$key}}" {{$product->product_unit === $key ? 'selected' : ''}}>{{$unit}}</option>
                                            @endforeach
                                        </select>
                                        @error('product_unit')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Discount price<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="discount_price" value="{{number_format($product->discount_price,0,"","")}}"  id="discount_price"  class="form-control" />
                                      @error('discount_price')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Product thumbnail<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="file" onchange="upload(this)"   id="product_thumbnail" class="form-control" />
                                      @error('file')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                    <div><img src="{{asset(count($product->mainImage) ? $product->mainImage[0]->url : "")}}" width="100" height="100" id="showImage" alt=""></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Multi images<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file"   name="files[]" multiple  onchange="uploadMulti(this)"  class="form-control" >
                                    </div>
                                    <div class="showMultiImage">
                                        @forelse ($product->subImages as $sub)
                                            <img src="{{asset($sub->url)}}" width="100" height="100" id="showImage" alt="">
                                        @empty
                                            <p>Rỗng</p>
                                        @endforelse
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row --}}
{{-- start row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h5>Short description vn<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea id="short_descp_vn" name="short_descp_vn" class="form-control" required="">
                                            {!! $product->short_descp_vn !!}
                                        </textarea>
                                      @error('short_descp_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h5>Long description vn<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea id="editor1" name="long_descp_vn" class="form-control" required="">
                                            {!! $product->long_descp_vn !!}
                                        </textarea>
                                        @error('long_descp_vn')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div> {{-- end row  --}}
                        <div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="controls">
										<fieldset>
											<input type="checkbox" name="hot_deals" value="1" {{$product->hot_deals == 1 ? 'checked' : ''}} id="checkbox_4" >
											<label for="checkbox_4">Hot deals</label>
										</fieldset>
							        </div>
                                </div>
                            </div>
                        </div>
                  </div>
                    <div class="text-xs-right">
                        <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Edit Product">
                    </div>
                </form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).ready(function() {

    });

</script>
<script>

</script>
<script type="text/javascript">
  function upload(input) {
   if(input.files && input.files[0]) {
       var file = input.files[0];
       var reader = new FileReader();
       reader.onload = function(e) {
            $('#showImage').attr('src', e.target.result).width(80).height(80);
       }
       reader.readAsDataURL(file);
   }
  }
$(document).on('change', '.choose_Image', function(e) {
    var id = $(this).data('id');
        var file = this.files[0]
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.showImage_'+id).attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
})
  function uploadMulti(input) {

      if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
      {
          $('.showMultiImage').empty();
          var data = input.files; //this file data

          $.each(data, function(index, file){ //loop though each file
              if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                  var fRead = new FileReader(); //new filereader
                  fRead.onload = function(e) {
                      var img = $('<img/>').addClass('thumb').attr('src', e.target.result) .width(80).height(80);//create image element
                      $('.showMultiImage').append(img); //append image to output element
                  }
                  fRead.readAsDataURL(file); //URL representing the file's data.
              }
              else {
                  alert("Your browser doesn't support File API!"); //if File API is absent
              }
          });

      }else{
          alert("Your browser doesn't support File API!"); //if File API is absent
      }
  }

</script>
  @endsection

