@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Main content -->
    <section class="content">
     <!-- Basic Forms -->
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">Add product</h4>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
                <form method="POST"  action="{{route('admin.product.save')}}" enctype="multipart/form-data">
                    @csrf
                  <div class="row">
                    <div class="col-12">
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Select category <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="category_id" id="category_id" class="form-control choose" required="">
                                        <option  selected="" disabled>Select category</option>
                                        @foreach ($categories as $category)
                                          <option value="{{$category->id}}">{{$category->category_name_vn}}</option>
                                        @endforeach
                                      </select>
                                      @error('category_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Selling price<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text"  name="selling_price" id="selling_price" required="" class="form-control" >
                                        @error('selling_price')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Product quantity <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_qty" required="" id="product_qty" class="form-control" >
                                        @error('product_qty')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Product Origin <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select name="country_id" id="country_id" class="form-control choose" required="">
                                            <option  selected="" disabled>Select origin</option>
                                            @foreach ($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('country_id')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div> {{-- end row  --}}
                        @livewire("slug-product", ["product"=> new App\Models\Product()])
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Dung tích<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input min="100" max="2000" type="text" name="capacity" value="{{old("capacity")}}"  id="capacity" class="form-control" />
                                        @error('capacity')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Unit<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select name="product_unit" id="product_unit" class="form-control choose" required="">
                                            @foreach( $product_units as $key=>$unit)
                                                <option value="{{$key}}">{{$unit}}</option>
                                            @endforeach
                                        </select>
                                        @error('product_unit')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Discount price<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="discount_price" value="{{old("discount_price")}}"  id="discount_price" class="form-control" />
                                        @error('discount_price')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Product thumbnail<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="file" onchange="upload(this)" required=""  id="product_thumbnail" class="form-control" />
                                      @error('product_thumbnail')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                    <div><img src="" id="showImage" alt=""></div>
                                  </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <h5>Multi images<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file"   name="files[]" multiple  onchange="uploadMulti(this)" required=""  class="form-control" >
                                    </div>
                                    <div class="showMultiImage">

                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}
                        {{-- start row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h5>Short description vn<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea id="short_descp_vn" name="short_descp_vn" class="form-control" required="">
                                            {{old("short_descp_vn")}}
                                        </textarea>
                                      @error('short_descp_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>

                        </div> {{-- end row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h5>Long description vn<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea id="editor1" name="long_descp_vn" class="form-control" required="">
                                            {{old("long_descp_vn")}}
                                        </textarea>
                                      @error('long_descp_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}

                        <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="controls">
										<fieldset>
											<input type="checkbox" name="hot_deals" value="1" id="checkbox_4" >
											<label for="checkbox_4">Hot deals</label>
										</fieldset>
							        </div>
                                </div>
                            </div>
                        </div>
                  </div>
                    <div class="text-xs-right">
                        <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Add Product">
                    </div>
                </form>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).ready(function() {

    });

</script>
<script>

</script>
<script type="text/javascript">
  function upload(input) {
   if(input.files && input.files[0]) {
       var file = input.files[0];
       var reader = new FileReader();
       reader.onload = function(e) {
            $('#showImage').attr('src', e.target.result).width(80).height(80);
       }
       reader.readAsDataURL(file);
   }
  }

  function uploadMulti(input) {

      if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
      {
        $('.showMultiImage').empty();
          var data = input.files; //this file data

          $.each(data, function(index, file){ //loop though each file
              if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                  var fRead = new FileReader(); //new filereader
                    fRead.onload = function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result) .width(80).height(80);//create image element
                        $('.showMultiImage').append(img); //append image to output element
                    }
                  fRead.readAsDataURL(file); //URL representing the file's data.
              }
              else {
                  alert("Your browser doesn't support File API!"); //if File API is absent
              }
          });

      }else{
          alert("Your browser doesn't support File API!"); //if File API is absent
      }
  }

</script>
  @endsection

