@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List responses from clients</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Created At</th>
                               <th>Name</th>
                               <th>Email</th>
                               <th>Phone</th>
                               <th>content</th>
                               <th>Rating</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($contacts as $contact)
                        <tr>
                            <td>{{Carbon\Carbon::parse($contact->created_at)->format('D, d/m/Y')}}</td>
                            <td>{{$contact->name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->phone}}</td>
                            <td>{{$contact->content}}</td>
                            <td>{{$contact->star ? $contact->star." Star" : "NULL"}}</td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

