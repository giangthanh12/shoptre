@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Edit category</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('admin.category.update', $category->id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Category name vn <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="category_name_vn" id="category_name_vn" value="{{$category->category_name_vn}}" class="form-control">
                                    @error('category_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Category icon <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="category_icon" value="{{$category->category_icon}}" id="category_icon" class="form-control">
                                    @error('category_icon')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Update Category">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

