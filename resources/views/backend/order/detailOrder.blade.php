@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-6">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Chi tiết người nhận</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table  class="table table-bordered table-striped">
                            <tr>
                                <td>Tên người nhận</td>
                                <td>{{$order->name}}</td>
                            </tr>
                            <tr>
                                <td>Số điện thoại</td>
                                <td>{{$order->phone}}</td>
                            </tr>
                            <tr>
                                <td>Email người nhận</td>
                                <td>{{$order->email}}</td>
                            </tr>
                            <tr>
                                <td>Địa chỉ người nhận</td>
                                <td>{{$order->address}}</td>
                            </tr>
                            <tr>
                                <td>Mã bưu điện</td>
                                <td>{{$order->post_code}}</td>
                            </tr>
                            {{-- <tr>
                                <td>Ngày đặt hàng</td>
                                <td>{{$order->order_date}}</td>
                            </tr> --}}
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
        <div class="col-6">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Chi tiết đơn hàng</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table  class="table table-bordered table-striped">
                        <tr>
                            <td>Người đặt hàng</td>
                            <td>{{$order->name}}</td>
                        </tr>
                        <tr>
                            <td>Số điện thoại người đặt</td>
                            <td>{{$order->phone}}</td>
                        </tr>
                        <tr>
                            <td>Hình thức đặt hàng</td>
                            <td>{{$payment_types[$order->payment_method]}}</td>
                        </tr>
                        <tr>
                            <td>Mã đơn hàng</td>
                            <td>{{$order->order_number}}</td>
                        </tr>
                        <tr>
                            <td>Tổng đơn hàng</td>
                            <td>{{number_format($order->amount,0,'','.')}}₫</td>
                        </tr>
                        <tr>
                            <td>Trạng thái đơn hàng</td>
                            <td><span>{{$order_status[$order->status]}}</span></td>



                        </tr>
                        <tr>
                            <td>Cập nhật trạng thái</td>
                            @if ($order->status == 1)
                            <td>
                                <a href="{{url('/admin/order/pending/processing/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Processing Order</a>
                                <a href="{{url('/admin/order/update-cancel/'.$order->id)}}" id="confirmed" class="btn btn-danger mb-5" >Update cancel Order</a>
                            </td>
                            @elseif ($order->status == 2)
                            <td>
                                <a href="{{url('/admin/order/processing/shipped/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update shipped Order</a>
                                <a href="{{url('/admin/order/update-cancel/'.$order->id)}}" id="confirmed" class="btn btn-danger mb-5" >Update cancel Order</a>
                            </td>
                            @elseif ($order->status == 3)
                            <td>
                                <a href="{{url('/admin/order/shipped/delivered/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update delivered Order</a>
                                <a href="{{url('/admin/order/update-cancel/'.$order->id)}}" id="confirmed" class="btn btn-danger mb-5" >Update cancel Order</a>
                            </td>
                            @else
                            <td><a href="{{url('/admin/order/delivered/return/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update return Order</a>
                                <a href="{{url('/admin/order/update-cancel/'.$order->id)}}" id="confirmed" class="btn btn-danger mb-5" >Update cancel Order</a>
                            </td>
                            @endif
                        </tr>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>


      <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Chi tiết sản phẩm</h3>
                    </div>
                <table class="table">
                    <div class="lds-ripple1"><div></div><div></div></div>
                    <thead>
                        <tr>
                            <th class="cart-description item">Ảnh</th>
                            <th class="cart-product-name item" >Tên sản phẩm</th>
                            <th class="cart-product-color item" width="20%">Mã code sản phẩm</th>
                            <th class="cart-product-size item">Xuất sứ</th>
                            <th class="cart-edit item" width="10%">Dung tích</th>
                            <th class="cart-price item">Giá</th>
                            <th class="cart-qty item" width="10%">Số lượng</th>
                            <th class="cart-sub-total item">Tổng</th>

                        </tr>
                    </thead><!-- /thead -->

                    <tbody>
                        @foreach ($order_products as $order_product)
                            <tr>
                                <td><img src="{{asset($order_product->product->mainImage[0]->url)}}" width="60" height="60" alt=""></td>
                                <td>{{$order_product->product->product_name_vn}}</td>
                                <td>{{$order_product->product->product_code}}</td>
                                <td>{{optional($order_product->product->country)->name}}</td>
                                <td>{{$order_product->product->capacity . " " . $product_units[$order_product->product->product_unit]}}</td>
                                <td>{{number_format($order_product->price,0,'','.')}}₫</td>
                                <td>{{$order_product->qty}}</td>
                                <td>{{number_format($order_product->price * $order_product->qty,0,'','.')}}₫</td>
                            </tr>
                        @endforeach
                    </tbody><!-- /tbody -->
                </table><!-- /table -->
                </div>
            </div>
      </div>


      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

