<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Hóa đơn</title>

<style type="text/css">
    * {
        font-family: DejaVu Sans;

    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
    .font{
      font-size: 15px;
    }
    .authority {
        /*text-align: center;*/
        float: right
    }
    .authority h5 {
        margin-top: -10px;
        color: green;
        /*text-align: center;*/
        margin-left: 35px;
    }
    .thanks p {
        color: green;;
        font-size: 16px;
        font-weight: normal;
        font-family: serif;
        margin-top: 20px;
    }
</style>

</head>
<body>

  <table width="100%" style="background: #F7F7F7; padding:0 20px 0 20px;">
    <tr>
        <td valign="top">
          <!-- {{-- <img src="" alt="" width="150"/> --}} -->
          <h2 style="color: green; font-size: 26px;"><strong>Shop Tre</strong></h2>
        </td>
        <td align="right">
            <pre class="font" >
               ShopTre<br>
               Email:LeHuyHoang2211@gmail.com <br>
               Số điện thoại: 0396820469 <br>
               Triều Khúc, Thanh Trì, Hà Nội <br>
            </pre>
        </td>
    </tr>

  </table>


  <table width="100%" style="background:white; padding:2px;""></table>

  <table width="100%" style="background: #F7F7F7; padding:0 5 0 5px;" class="font">
    <tr>
        <td>
          <p class="font" style="margin-left: 20px;">
           <strong>Tên người nhận:</strong> {{$order->name}} <br>
           <strong>Email:</strong> {{$order->email}} <br>
           <strong>Số điện thoại:</strong> {{$order->phone}} <br>

           <strong>Địa chỉ:</strong> {{$order->address}} <br>
           <strong>Mã bưu điện:</strong> {{$order->post_code}}
         </p>
        </td>
        <td>
          <p class="font">
            <h3><span style="color: green;">Mã hóa đơn:</span> {{$order->invoice_no}}</h3>
            Ngày đặt hàng: {{\Carbon\Carbon::parse($order->created_at)->format("d-m-Y H:i")}} <br>
            @if ($order->confirmed_date != null)
            Ngày vận chuyển: {{$order->confirmed_date}} <br>
            @endif

            Loại thanh toán : {{$payment_types[$order->payment_method]}} </span>
         </p>
        </td>
    </tr>
  </table>
  <br/>
<h3>Sản phẩm</h3>


  <table width="100%">
    <thead style="background-color: green; color:#FFFFFF;">
      <tr class="font">
        <th>Hình ảnh</th>
        <th>Tên sản phẩm</th>
        {{-- <th>Kích cỡ</th>
        <th>Màu sắc</th> --}}
        <th>Code</th>
        <th>Số lượng</th>
        <th>Giá</th>
        <th>Tổng</th>
      </tr>
    </thead>
    <tbody>

@foreach ($order_products as $item)
    <tr class="font">
        <td align="center">
            <img src="{{public_path($item->product->mainImage[0]->url)}}" height="60px;" width="60px;" alt="">
        </td>
        <td align="center">{{$item->product->product_name_vn}}</td>
        <td align="center">{{$item->product->product_code}}</td>
        <td align="center">{{$item->qty}}</td>
        <td align="center">{{number_format($item->price,0,'','.')}}₫</td>
        <td align="center">{{number_format($item->qty*$item->price,0,'','.')}}₫</td>
    </tr>
@endforeach

    </tbody>
  </table>
  <br>
  <table width="100%" style=" padding:0 10px 0 10px;">
    <tr>
        <td align="right" >
            {{-- <h2><span style="color: green;">Tạm tính:</span> {{number_format($order->order_amount,0,'','.')}}</h2> --}}
            <h2><span style="color: green;">Tổng thanh toán:</span> {{number_format($order->amount,0,'','.')}} đ</h2>
            {{-- <h2><span style="color: green;">Full Payment PAID</h2> --}}
        </td>
    </tr>
  </table>
  <div class="thanks mt-3">
    <p style="font-family: DejaVu Sans;">Cảm ơn bạn đã mua hàng của chúng tôi.</p>
  </div>
  <div class="authority float-right mt-5">
      <p>-----------------------------------</p>
      <h5>Người nhận hàng:</h5>
    </div>
</body>
</html>
