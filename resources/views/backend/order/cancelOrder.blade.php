@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List cancel Orders</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>OrderId</th>
                                <th>Amount</th>
                                <th>Payment</th>
                                <th>Status</th>
                                <th width="20%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                         <tr>
                             <td>{{Carbon\Carbon::parse($order->created_at)->format('D, d/m/Y')}}</td>
                             <td>{{$order->order_number}}</td>
                             <td>{{number_format($order->amount,0,'','.')}}</td>
                             <td>
                                 @if ($order->payment_method == 1)
                                     <span class="badge badge-success" >Thanh toán tại nhà</span>
                                 @else
                                     <span class="badge badge-warning">Thanh toán online</span>
                                 @endif
                             </td>
                             <td>
                                 <span class="badge badge-danger">{{$order->status == 0 ?  "Đơn hàng thất bại"  : ""}}</span>
                             </td>

                             <td>
                                 <a href="{{route('admin.order.detail',$order->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-eye"></i></a>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

