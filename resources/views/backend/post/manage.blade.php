@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Posts</h3>
                 <a class="btn btn-primary float-right" href="{{route('Post.add')}}">Thêm bài viết</a>
               </div>

               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Post thumbnail</th>
                               <th>Post category</th>
                               <th>Post title vn</th>
                               <th>Post title en </th>
                               <th>Status</th>
                               <th>Created at</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($posts as $post)
                        <tr>
                            <td><img src="{{asset($post->post_image)}}" width="50" height="50" alt=""></td>
                            <td>{{$post->category->post_category_name_vn}}</td>
                            <td>{{$post->post_title_vn}}</td>
                            <td>{{$post->post_title_en}}</td>
                            <td>
                                @if ($post->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-warning">Inactive</span>
                                @endif
                            </td>
                            <td>{{$post->created_at}}</td>
                            <td>
                                <a href="{{route('Post.edit',$post->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('Post.delete',$post->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                                @if ($post->status == 1)
                                    <a href="{{route('Post.inactive', $post->id)}}" class="btn btn-danger mb-5" title="inactive"><i class="fa fa-arrow-down"></i></a>
                                @else
                                <a href="{{route('Post.active', $post->id)}}" class="btn btn-success mb-5" title="active"><i class="fa fa-arrow-up"></i></a>
                                @endif
                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

