@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Sửa bài viết</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                <form method="POST"  action="{{route('Post.update')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                  <div class="row">
                    <div class="col-12">
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h5>Danh mục bài viết <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="post_category_id" id="post_category_id" class="form-control" required="">
                                        <option  selected="" disabled>Chọn danh mục</option>
                                        @foreach ($categories as $category)
                                          <option {{$post->post_category_id === $category->id ? 'selected' : ''}} value="{{$category->id}}">{{$category->post_category_name_vn}}</option>
                                        @endforeach
                                      </select>
                                      @error('post_category_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h5>Post title vn <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" value="{{$post->post_title_vn}}" name="post_title_vn" id="post_title_vn" class="form-control" required="">
                                      @error('post_title_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}

                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h5>Post title en <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" value="{{$post->post_title_en}}" name="post_title_en" id="post_title_en" class="form-control" required="">
                                      @error('post_title_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Post image<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="post_image" onchange="upload(this)"   id="post_image" class="form-control" />
                                      @error('post_image')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                    <div><img src="{{asset($post->post_image)}}" id="showImage" alt=""></div>
                                  </div>
                            </div>


                        </div> {{-- end row  --}}



{{-- start row  --}}
<div class="row">{{-- start row  --}}
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Post detail vn<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea id="post_detail_vn" name="post_detail_vn" class="form-control" required="">
                    {{$post->post_detail_vn}}
                </textarea>
              @error('short_descp_vn')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Post detail en<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea id="post_detail_en" name="post_detail_en" class="form-control" required="">
                    {{$post->post_detail_en}}
                </textarea>
              @error('post_detail_en')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
    </div>

</div> {{-- end row  --}}

<div class="text-xs-right">
    <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Cập nhật bài viết">
</div>

                  </div>

                </form>


               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  <script type="text/javascript">
    function upload(input) {
     if(input.files && input.files[0]) {
         var file = input.files[0];
         var reader = new FileReader();
         reader.onload = function(e) {
              $('#showImage').attr('src', e.target.result).width(80).height(80);
         }
         reader.readAsDataURL(file);
     }
    }


  </script>
  @endsection

