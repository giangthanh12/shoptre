@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Main content -->
    <section class="content">

        <!-- Basic Forms -->
         <div class="box">
           <div class="box-header with-border">
             <h4 class="box-title">Chỉnh sửa thành viên</h4>

           </div>
           <!-- /.box-header -->
           <div class="box-body">
             <div class="row">
               <div class="col">
                   <form method="POST" action="{{route('Admin.update',$admin->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                       <div class="col-12">
                           <div class="row">
                               <div class="col-md-6">
                                   <div class="form-group">
                                        <h5>Tên<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{{$admin->name}}" class="form-control" > </div>
                                    </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                        <h5>Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" value="{{$admin->email}}" class="form-control" required="" >
                                        </div>
                                    </div>
                               </div>
                           </div>
                           <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                     <h5>Mật khẩu mới<span class="text-danger">*</span></h5>
                                     <div class="controls">
                                         <input type="password" name="password" class="form-control" >
                                     </div>
                                 </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     <h5>Điện thoại <span class="text-danger">*</span></h5>
                                     <div class="controls">
                                         <input type="number" value="{{$admin->phone}}" name="phone" class="form-control" required="" >
                                        </div>
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Hình ảnh <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" id="image" name="profile_photo_path" class="form-control" >
                                        </div>
                                    </div>
                               </div>
                               <div class="col-md-6">
                                    <img width="100" height="100" id="showImage" src="{{empty($admin->profile_photo_path) ? url('upload/user3-128x128.jpg'): asset($admin->profile_photo_path)}}" alt="">
                               </div>
                           </div>

                           <hr>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-outline-primary">
          <div class="box-header with-border">
            <h4 class="box-title"><strong>Phân quyền thành viên</strong></h4>
          </div>
          <div class="box-body">
            <div class="row">
                @foreach ($modules as $module)
                <div class="col-md-3">
                    <div class="form-group">
                        <h3>{{$module->name_module}}</h3>
                        <div class="controls">
                            <input type="hidden" name="module_id" value={{$module->id}}>
                            @php
                                $role = App\Models\Role::where('module_id', $module->id)->where('admin_id',$admin->id)->first();

                            @endphp
                            <fieldset>
                                <input type="checkbox" name="read{{$module->id}}" {{isset($role) && $role->readRole == 1 ? 'checked' : '' }} id="read{{$module->id}}" value="1"  >
                                <label for="read{{$module->id}}">Read</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="create{{$module->id}}" {{isset($role) && $role->createRole == 1 ? 'checked' : '' }} id="create{{$module->id}}" value="1" >
                                <label for="create{{$module->id}}">Create</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="edit{{$module->id}}" {{isset($role) && $role->editRole == 1 ? 'checked' : '' }} id="edit{{$module->id}}" value="1">
                                <label for="edit{{$module->id}}">Edit</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="delete{{$module->id}}" {{isset($role) && $role->deleteRole == 1 ? 'checked' : '' }} id="delete{{$module->id}}" value="1" >
                                <label for="delete{{$module->id}}">Delete</label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
          </div>
        </div>
      </div>
</div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-info" value="Cập nhật thành viên">
                        </div>
                       </div>





                   </form>

               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->

       </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    $(document).ready(function() {
        $('#image').change(function(e) {
           var reader = new FileReader(); // tạo một file đọc
           reader.onload = function(e) {
            // console.log(e.target.result);
            $('#showImage').attr('src', e.target.result);
           } // quá trình đọc kết thúc sẽ được gán giá trị kết quả cho src
           if (e.target.files[0]) {
                reader.readAsDataURL(e.target.files[0]);// chỉ ra đó là file đọc data url
            }
        })
    });
  </script>
@endsection

