@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add brand</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Site.update', $site->id)}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Số điện thoại 1<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="number" name="phone_one" value="{{$site->phone_one}}"  id="phone_one" class="form-control">
                                    @error('phone_one')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Số điện thoại 2<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="phone_two" value="{{$site->phone_two}}" id="phone_two" class="form-control">
                                    @error('phone_two')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Email<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="email" name="email" value="{{$site->email}}" id="email" class="form-control">
                                    @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Tên doanh nghiệp<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="company_name" value="{{$site->company_name}}" id="company_name" class="form-control">
                                    @error('company_name')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Địa chỉ doanh nghiệp<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="company_address" value="{{$site->company_address}}" id="company_address" class="form-control">
                                    @error('company_address')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Địa chỉ facebook<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="facebook" value="{{$site->facebook}}" id="facebook" class="form-control">
                                    @error('facebook')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Địa chỉ twitter<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="twitter" value="{{$site->twitter}}" id="twitter" class="form-control">
                                    @error('twitter')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Địa chỉ linkedin <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="linkedin" value="{{$site->linkedin}}" id="linkedin" class="form-control">
                                    @error('linkedin')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Địa chỉ youtube <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="youtube" value="{{$site->youtube}}" id="youtube" class="form-control">
                                    @error('youtube')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Logo<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="file" name="logo" id="logo" class="form-control">
                                    <input type="hidden"  name="logo"  value="{{$site->brand_image}}" class="form-control">
                                    @error('logo')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                                <div>
                                    <img id="showImage" src="{{!empty($site->logo) ? asset($site->logo) : url('upload/user3-128x128.jpg')}}" width="100" height="100" alt="">
                                </div>

                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Update site">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  <script>
     var  elementImage = document.querySelector('#logo');
     element = document.querySelector('#showImage');
       elementImage.onchange = function(e){
           var reader = new FileReader();
           reader.onload = function(e) {
            element.setAttribute('src',reader.result);
           }
           reader.readAsDataURL(elementImage.files[0]);
       };
  </script>
  @endsection

