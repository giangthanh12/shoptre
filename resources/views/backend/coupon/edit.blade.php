@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add coupon</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Coupon.update', $coupon->id)}}"  method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Coupon name vn <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="coupon_name_vn" value="{{$coupon->coupon_name_vn}}" id="coupon_name_vn" class="form-control">
                                    @error('coupon_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group">
                                <h5>Coupon discount (%) <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="coupon_discount" value="{{$coupon->coupon_discount}}" id="coupon_discount" class="form-control">
                                    @error('coupon_discount')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Coupon validate <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="date" name="coupon_validate" value="{{$coupon->coupon_validate}}" id="coupon_validate" class="form-control">
                                    @error('coupon_validate')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Coupon">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

