@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Brands</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Coupon name vn</th>
                               <th>Coupon discount</th>
                               <th>Coupon validate</th>
                               <th>Status</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($coupons as $coupon)
                        <tr>
                            <td>{{$coupon->coupon_name_vn}}</td>
                            <td>{{$coupon->coupon_discount}} %</td>
                            <td>{{Carbon\Carbon::parse($coupon->coupon_validate)->format('D, d/m/Y')}}</td>
                            <td>
                                @if ( $coupon->status == 1 && $coupon->coupon_validate >= Carbon\Carbon::now())
                                <span class="badge badge-success">Valid</span>
                                @else
                                <span class="badge badge-warning">Invalid</span>
                                @endif
                            </td>

                            <td>
                                <a href="{{route('Coupon.edit',$coupon->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('Coupon.delete',$coupon->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add brand</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Coupon.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Coupon name vn <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="coupon_name_vn" id="coupon_name_vn" class="form-control">
                                    @error('coupon_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Coupon discount <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="number" name="coupon_discount" id="coupon_discount" class="form-control">
                                    @error('coupon_discount')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Coupon validate<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="date" name="coupon_validate" id="coupon_validate" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control">
                                    @error('coupon_validate')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Coupon">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

