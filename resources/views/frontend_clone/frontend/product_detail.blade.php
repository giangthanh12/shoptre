@extends("frontend_clone.layouts.master")
@section("content")
<div class = "card-wrapper product-detail">
    <div class = "card">
        <!-- card left -->
        <div class = "product-imgs">
            <div class = "img-display">
                <div class = "img-showcase">
                    @foreach($product->images as $image)
                    <img  src = "{{asset($image->url)}}" alt = "shoe image">
                    @endforeach
                </div>
            </div>
            <div class = "img-select">
                @foreach($product->images as $key=>$image)
                    <div class = "img-item">
                        <a href = "#" data-id = "{{$key+1}}">
                            <img style="height: 140px;width: 140px;" src = "{{asset($image->url)}}" alt = "shoe image">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- card right -->
        <div class = "product-content">
            <h4 class = "product-title">{{$product->product_name_vn}}</h4>
            <a href = "{{route("client.category", $product->category->category_slug_vn)}}" class = "product-link">Xem nhiều sản phẩm hơn nữa tại đây</a>

            <div class = "product-price">
                <p class = "last-price">Giá cũ: <span>{{number_format($product->selling_price,0,",",",")}} Đ</span></p>
                <p class = "new-price">Giá mới: <span>{{number_format($product->discount_price,0,",",",")}} Đ</span></p>
            </div>

            <div class = "product-detail">
                <h2>Giới thiệu sản phẩm: </h2>
                <p>{!! $product->short_descp_vn !!}</p>
                <ul>
                    <li>Xuất xứ: <span>{{optional($product->country)->name}}</span></li>
                    <li>Dung tích: <span>{{$product->capacity}} {{\App\Models\Product::UNIT[$product->product_unit]}}</span></li>
                    <li>Tình trạng: <span>{{$product->product_qty > 0 ? "Còn hàng" : "Hết hàng"}}</span></li>
                    <li>Số lượng: <span>{{$product->product_qty}}</span></li>
                    <li>Tình trạng: <span>{{$product->product_qty > 0 ? "Còn hàng" : "Hết hàng"}}</span></li>
                    <li>Danh mục: <span>{{$product->category->category_name_vn}}</span></li>
                    <li>Vận chuyển: <span>Khu vực Hà Nội</span></li>
                    <li>Phí vận chuyển: <span>Free</span></li>
                </ul>
            </div>

            <div class = "purchase-info">
                <input id="qty-product" type = "number" min = "0" value = "1">
                <button type = "button" onclick="addToCart(event, {{$product->id}},$('#qty-product').val())" class = "btn">
                    Thêm giỏ hàng <i class = "fas fa-shopping-cart"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="container pad-1-lr">
    <div class="new-products__table__header text-tran">
        <h3 style="margin: 0;">Nội dung sản phẩm</h3>
    </div>
    <div class="content">
        {!! $product->long_descp_vn !!}
    </div>
</div>
<div class="best-sell__box container pad-1-lr" style="margin-top: 100px;">
    <div class="best-sell__box__header">
        <h4
        style="
            font-size: 2rem;
            color: #0098d2;
            text-align: center;
            background: #cbecf7;
            padding: 15px;
        "
        >Sản phẩm liên quan</h4>
    </div>
    <ul id="content-slider" class="content-slider">
        @forelse($productsRelated as $productRelated)
            <li>
                <a class="table__body__link border-left-none" href="{{route("client.products.detail",$productRelated->product_slug_vn)}}">
                    <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                            <div class="img__inside pos-rela">
                                <img class="table__img" width="180" height="180" src="{{asset($productRelated->mainImage[0]->url)}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ">
                            </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao </h4>
                        @if(!is_null($productRelated->discount_price) && $productRelated->discount_price > 0)
                            <div class="price-show">
                                <p class="products__price" style="font-size:14px;text-decoration: line-through; color: #808080ba;">{{number_format($productRelated->selling_price,0,",",",")}} Đ</p>
                                <p class="products__price" style="font-size:14px;">{{number_format($productRelated->discount_price,0,",",",")}} Đ</p>
                            </div>
                        @else
                            <p class="products__price" style="padding-top: 15px; font-size:14px;" >{{number_format($productRelated->selling_price,0,",",",")}} Đ</p>
                        @endif
                    </div>
                </a>
            </li>
        @empty
            <h4>Rỗng</h4>
        @endforelse

    </ul>
</div>
<script>
    $(document).ready(function() {
        $("#content-slider").lightSlider({
            auto:true,
            item:6,
            loop:true,
            keyPress:true
        });
    })
</script>
@endsection

{{--<script src="script.js"></script>--}}
{{--</body>--}}
{{--</html>--}}

