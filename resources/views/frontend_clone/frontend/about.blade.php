@extends("frontend_clone.layouts.master")
@push("css")
<link rel="stylesheet" href="{{asset('frontend_clone/css/about.css')}}">
@endpush
@section("content")
<style>
    body {font-family: "Times New Roman", Georgia, Serif;}
    h1, h2, h3, h4, h5, h6 {
        font-family: "Playfair Display";
    }
    * {
        box-sizing: unset !important;
    }
    .star{
    border:none;
    background-color: unset;
    color:goldenrod;
    font-size: 3rem;
    }
</style>
<div class="w3-content" style="max-width:1100px">

    <!-- About Section -->
    <div class="w3-row w3-padding-64" id="about" style="display: flex;">
      <div class="w3-col m6 w3-padding-large w3-hide-small">
       <img src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2022/5/31/1051007/Sua-Lua-1.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
      </div>

      <div class="w3-col m6 w3-padding-large">
        <h1 class="w3-center">Sản phẩm của chúng tôi</h1><br>
        <h5 class="w3-center">Từ 2018</h5>
        <p class="w3-large">Luôn tiên phong trong các xu hướng dinh dưỡng tiên tiến trên thế giới để hướng đến sự phát triển cả về thể chất lẫn trí tuệ của các thế hệ người Việt, Công ty sữa chúng tôi hiện có hơn 250 loại sản phẩm được đa dạng hóa nhằm phục vụ các nhu cầu của người tiêu dùng</p>
        <p class="w3-large w3-text-grey w3-hide-medium">Chúng tôi tin tưởng rằng một quy trình quản lý hiệu quả là nền tảng cho sự phát triển, mang lại sự vững mạnh về tài chính, lòng tin cho nhà đầu tư và hiệu quả cho hoạt động doanh nghiệp.</p>
      </div>
    </div>

    <hr>

    <!-- Menu Section -->
    <div class="w3-row w3-padding-64" id="menu" style="display: flex;">
      <div class="w3-col l6 w3-padding-large">
        <h1 class="w3-center">Tầm nhìn chúng tôi</h1><br>
        <h4>Chính trực</h4>
        <p class="w3-text-grey">Liêm chính, Trung thực trong ứng xử và trong tất cả các giao dịch.</p><br>

        <h4>Tôn trọng</h4>
        <p class="w3-text-grey">Tôn trọng bản thân, Tôn trọng đồng nghiệp, Tôn trọng Công ty, Tôn trọng đối tác, Hợp tác trong sự tôn trọng</p><br>

        <h4>Công bằng</h4>
        <p class="w3-text-grey">Công bằng với nhân viên, khách hàng, nhà cung cấp và các bên liên quan khác.</p><br>

        <h4>Đạo đức</h4>
        <p class="w3-text-grey">Tôn trọng các tiêu chuẩn đã được thiết lập và hành động một cách đạo đức.</p><br>


        <h4>Tuân thủ</h4>
        <p class="w3-text-grey">Tuân thủ Luật pháp, Bộ Quy Tắc Ứng Xử và các quy chế, chính sách, quy định của Công ty.</p>
      </div>

      <div class="w3-col l6 w3-padding-large">
        <img src="https://www.associatedcollectors.com/portals/0/Images/values-mission-vision.jpg" class="w3-round w3-image w3-opacity-min" alt="Menu" style="width:100%">
      </div>
    </div>

    <hr>

    <!-- Contact Section -->
    <div class="w3-container w3-padding-64" id="contact">
      <h1>Liên hệ</h1><br>
      <p></p>
      <p class="w3-text-blue-grey w3-large"><b>Liên hệ chúng tôi tại số nhà 12 Triều Khúc, Thanh Xuân, Hà Nội</b></p>
      <p>Bạn có thể gọi tôi qua số điện thoại 0396820467 or email LocTran@gmail.com, hoặc bạn có thể liên hệ chúng tôi tại đây:</p>
      <form action="{{route("client.contact")}}"  method="post" >
        @csrf
        <p><input class="w3-input w3-padding-16" type="text" placeholder="Tên của bạn" required name="name"></p>
        <p><input class="w3-input w3-padding-16" type="text" placeholder="Email của bạn" required name="email"></p>
        <p><input class="w3-input w3-padding-16" type="number" placeholder="Số điện thoại" required name="phone"></p>
        <p><input class="w3-input w3-padding-16" type="text" placeholder="Nội dung phản hồi" required name="content"></p>
        <p><input class="w3-input w3-padding-16" type="hidden" value="" required id="star" name="star"></p>
        <div class="container" style="margin-top:10px;">
            <button type="button" class="star">&#9734;</button>
            <button type="button" class="star">&#9734;</button>
            <button type="button" class="star">&#9734;</button>
            <button type="button" class="star">&#9734;</button>
            <button type="button" class="star">&#9734;</button>
        </div>
        <p><button class="w3-button w3-light-grey w3-section" type="submit">Gửi</button></p>
      </form>
    </div>

  <!-- End page content -->
  </div>
  <script>
    $(document).ready(function () {
        const stars=document.querySelectorAll('.star');
        const current_rating=document.querySelector('.current-rating');

        stars.forEach((star,index)=>{
        star.addEventListener('click',()=>{

            let current_star=index+1;
            $("#star").val(current_star);
            stars.forEach((star,i)=>{
                if(current_star>=i+1){
                star.innerHTML='&#9733;';
                }else{
                star.innerHTML='&#9734;';
                }
            });

        });
        });
    });
  </script>
@endsection
