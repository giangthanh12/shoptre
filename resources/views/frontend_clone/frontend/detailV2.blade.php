@extends("frontend_clone.layouts.masterV2")
@section("content")

<style>

    * {
      padding: 0;
      margin: 0;
      transition: ease 0.5s;
      text-decoration: none;
    }

    .product-card {
      background: white;
      width: 100%;
      max-width: 1200px;
      margin: 0 auto;
      margin-top: 50px;
      padding: 30px;
      border-radius: 20px;
      box-shadow: #dbe2ee 0 0 20px 5px;
      display: flex;
      gap: 30px;
      color: #191847;
      margin-bottom: 50px
    }

    .product-info{
        margin-left: 8%
    }

    .cover-image {
      width: 400px;
      height: 400px;
      overflow: hidden;
    }
    img:hover{
      transform: scale(1.05);
    }
    .more-image-container {
      display: flex;
    }
    .more-image {
      width: 100px;
    }
    .image-container img {
      width: 100%;
      object-fit: cover;
    }

    .free-shipping {
      display: inline-block;
      background: #00adef;
      padding: 5px 10px;
      border-radius: 30px;
      font-size: 14px;
      color: white;
      font-weight: 400;
    }

    .product-name {
      margin: 10px 0 20px 0;
      font-size: 22px;
    }

    .regular-price {
      text-decoration: line-through;
    }

    .discount-price {
      display: inline-block;
      font-size: 30px;
      font-weight: 700;
    }

    .offer-info {
      font-size: 14px;
      color: gray;
    }

    .add-to-cart {
      display: inline-block;
      width: 68%;
      background: #00adef;
      color: white;
      padding: 15px;
      border-radius: 8px;
      font-size: 18px;
      font-weight: 500;
      text-align: center;
      box-shadow: #e2e9f3 0 5px;
      margin: 20px 0;
    }
    .add-to-cart ion-icon {
      font-size: 30px;
      margin-bottom: -8px;
    }
    .add-to-cart:hover {
      background: #e2e9f3;
      box-shadow: #00adef 0 5px;
      color: #00adef;
    }
    .add-to-cart:hover ion-icon {
      transform: rotate(360deg);
    }

    .stock {
      display: flex;
      align-items: center;
      gap: 10px;
    }
    .stock-status {
      width: 14px;
      height: 14px;
      background: #00d98b;
      border-radius: 50%;
    }
    .stock-info {
      font-size: 14px;
      font-weight: 600;
      color: black;
    }

    .btn-option-detail {
      display: flex;
      margin-top: 20px;
    }

    .button-option {
      width: 200px;
      color: #00adef;
      font-size: 18px;
      font-weight: 500;
      text-align: center;
      padding: 15px;
      border: solid #dfe6f0 2px;
      border-radius: 10px;
    }
    .button-option ion-icon {
      font-size: 30px;
      margin-bottom: -7px;
    }
    .button-option:hover {
      box-shadow: #e2e9f3 0 0 10px;
    }
    .button-option:hover ion-icon {
      transform: rotate(360deg);
    }
    </style>


<div class="product-card">
    <div class="image-container">
      <div class="cover-image product-image">
        <img src="{{asset('frontend_clone/img/new_product/1_29.jpg')}}" alt="">
      </div>
      <div class="more-image-container">
        <div class="more-image product-image">
           <img src="{{asset('frontend_clone/img/new_product/1_29.jpg')}}" alt="">
        </div>
        <div class="more-image product-image">
            <img src="{{asset('frontend_clone/img/new_product/3_30.jpg')}}" alt="">
        </div>
        <div class="more-image product-image">
            <img src="{{asset('frontend_clone/img/new_product/1_29.jpg')}}" alt="">
        </div>
        <div class="more-image product-image">
            <img src="{{asset('frontend_clone/img/new_product/1_29.jpg')}}" alt="">
        </div>
      </div>
    </div>
    <div class="product-info">
      <a href="#" class="free-shipping">Free shipping</a>
      <h3 class="product-name">Tên sản phẩm</h3>
      <p class="regular-price">120.000đ</p>
      <p class="discount-price">100.000đ</p>
      <p class="offer-info">abcxyz</p>
      <a href="#" class="add-to-cart">
        <ion-icon name="add-outline"></ion-icon> Add to cart
      </a>
      <div class="stock">
        <div class="stock-status"></div>
        <p class="stock-info">50 cái trong kho</p>
      </div>
      <div class="btn-option-detail">
        <a href="#" class="button-option" style="margin-right:24px">
          <ion-icon name="bag-add-outline"></ion-icon> Add to cart
        </a>
        <a href="#" class="button-option">
          <ion-icon name="heart-outline"></ion-icon> Add to wishlist
        </a>
      </div>
    </div>
  </div>

  <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
  <script>

    var arrImg =  Array.from(document.querySelectorAll(".more-image"));
    /*  Eventlistener for  image to change when image button is clicked  */
    arrImg.forEach((btn, i) => {
     btn.addEventListener("click", function () {
        let attrImg = $(btn.querySelectorAll("img")).attr('src');
        $(".cover-image>img").attr('src',attrImg);
      });
    });
  </script>
@endsection
