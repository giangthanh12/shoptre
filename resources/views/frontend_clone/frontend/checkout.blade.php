
@extends("frontend_clone.layouts.master")
<style>
    html,
    body {
        font-family: 'Montserrat', sans-serif;
        font-size-adjust: u
        display: flex;
        width: 100%;
        height: 100%;
        background: #eeeeee;
        justify-content: center;
        align-items: center;
    }
    .checkout-panel {
        display: flex;
        flex-direction: column;
        width: 940px;
        background-color: rgb(255, 255, 255);
        box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .2);
    }
    .panel-body {
        padding: 45px 40px 0;
        flex: 1;
    }

    .title {
        font-weight: 700;
        margin-top: 0;
        margin-bottom: 40px;
        color: #2e2e2e;
    }
    .progress-bar {
        display: flex;
        margin-bottom: 50px;
        justify-content: space-between;
    }

    .step {
        box-sizing: border-box;
        position: relative;
        z-index: 1;
        display: block;
        width: 25px;
        height: 25px;
        margin-bottom: 30px;
        border: 4px solid #fff;
        border-radius: 50%;
        background-color: #efefef;
    }

    .step:after {
        position: absolute;
        z-index: -1;
        top: 5px;
        left: 22px;
        width: 225px;
        height: 6px;
        content: '';
        background-color: #efefef;
    }

    .step:before {
        color: #2e2e2e;
        position: absolute;
        top: 40px;
    }

    .step:last-child:after {
        content: none;
    }

    .step.active {
        background-color: #1abc9c;
    }
    .step.active:after {
        background-color: #1abc9c;
    }
    .step.active:before {
        color: #1abc9c;
    }
    .step.active + .step {
        background-color: #1abc9c;
    }
    .step.active + .step:before {
        color: #1abc9c;
    }

    .step:nth-child(1):before {
        content: 'Cart';
    }
    .step:nth-child(2):before {
        right: -40px;
        content: 'Confirmation';
    }
    .step:nth-child(3):before {
        right: -30px;
        content: 'Payment';
    }
    .step:nth-child(4):before {
        right: 0;
        content: 'Finish';
    }
    .payment-method {
        display: flex;
        margin-bottom: 60px;
        justify-content: space-between;
    }

    .method {
        display: flex;
        flex-direction: column;
        width: 382px;
        height: 122px;
        padding-top: 20px;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 2px;
        background-color: rgb(249, 249, 249);
        justify-content: center;
        align-items: center;
    }

    .card-logos {
        display: flex;
        width: 150px;
        justify-content: space-between;
        align-items: center;
    }

    .radio-input {
        margin-top: 20px;
    }

    input[type='radio'] {
        display: inline-block;
    }
    .input-fields {
        display: flex;
        justify-content: space-between;
        gap: 10px;
    }

    .input-fields label {
        margin-bottom: 10px;
        color: #b4b4b4;
    }

    .info {
        font-size: 12px;
        font-weight: 300;
        display: block;
        margin-top: 50px;
        opacity: .5;
        color: #2e2e2e;
    }

    div[class*='column'] {
        width: 382px;
    }

    input[type='text'],
    input[type='password'] {
        font-size: 16px;
        width: 100%;
        height: 50px;
        padding-right: 40px;
        padding-left: 16px;
        color: rgba(46, 46, 46, .8);
        border: 1px solid rgb(225, 225, 225);
        border-radius: 4px;
        outline: none;
    }

    input[type='text']:focus,
    input[type='password']:focus {
        border-color: rgb(119, 219, 119);
    }

    #date { background: url(img/icons_calendar_black.png) no-repeat 95%; }
    #cardholder { background: url(img/icons_person_black.png) no-repeat 95%; }
    #cardnumber { background: url(img/icons_card_black.png) no-repeat 95%; }
    #verification { background: url(img/icons_lock_black.png) no-repeat 95%; }

    .small-inputs {
        display: flex;
        margin-top: 20px;
        justify-content: space-between;
    }

    .small-inputs div {
        width: 182px;
    }
    .panel-footer {
        display: flex;
        width: 100%;
        height: 96px;
        background-color: #fff;
        justify-content: center;
        align-items: center;
    }
    .btn {
        font-size: 16px;
        width: 163px;
        height: 48px;
        cursor: pointer;
        transition: all .2s ease-in-out;
        letter-spacing: 1px;
        border: none;
        border-radius: 23px;
        width: 100%;
    }

    .back-btn {
        color: #1abc9c;
        background: #fff;
    }

    .next-btn {
        color: #fff;
        background: #1abc9c;
    }

    .btn:focus {
        outline: none;
    }

    .btn:hover {
        transform: scale(1.1);
    }
    .blue-border {
        border: 1px solid rgb(110, 178, 251);
    }
    .warning {
        border-color: #1abc9c;
    }
</style>
@section("content")
<div class="container" style="display: flex;">


    <div class="checkout-panel">
        <form action="{{route("client.checkout")}}" method="POST" >
        <div class="panel-body">
                @csrf
                <h2 class="title">Thanh toán tại đây</h2>
                <div class="input-fields">
                    <div class="column-1" >
                        <label for="cardholder">Tên người nhận</label>
                        <input required type="text" id="name" name="name" />
                    </div>
                    <div class="column-2">
                        <label for="cardnumber">Số điện thoai</label>
                        <input  required type="text" name="phone" id="phone"/>
                    </div>
                </div>
                <div class="input-fields">
                    <div class="column-1">
                        <label for="cardholder">Email</label>
                        <input required type="text" name="email" id="email" />
                    </div>
                    <div class="column-2">
                        <label for="cardnumber">Mã bưu điện</label>
                        <input required type="text" id="post_code" name="post_code"/>
                    </div>
                </div>
                <div class="input-fields" >
                    <div class="column-2" style="    width: 100%;">
                        <label for="cardnumber">Thông tin chi tiết</label>
                        <textarea required style="width: 100%;" name="address" rows="10" id="" ></textarea>
                    </div>
                </div>
                <div class="input-fields" style="display: block">
                    <div class="column-2" style="    width: 100%;">
                        <label for="cardnumber">Phương thức thanht toán</label>
                    </div>

                    <div class="column-2" style="display:flex; width:100%;">
                        <div class="form-group">
                            <input checked type="radio" id="cod" value="1" name="payment_method" >
                            <label for="cod">Thanh toán tại nhà</label>
                        </div>
                        <div class="form-group">
                            <input type="radio" id="qr" value="2" name="payment_method">
                            <label for="qr">Thanh toán qua QR</label>
                        </div>
                        <div class="form-group">
                            <input type="radio" id="qtm" value="3" name="payment_method" >
                            <label for="qtm">Thanh toán qua ATM</label>
                        </div>
                    </div>

                </div>


        </div>

        <div class="panel-footer">
            <button style="width: 80%;" type="submit" class="btn next-btn">Tiến hành thanh toán</button>
        </div>
        </form>
    </div>
    <div class="checkout-panel">
        <div class="panel-body" style="padding:5px;">
            <h2 class="title">Thông tin giỏ hàng</h2>
            <ul class="cd-cart-items">
                @foreach($dataCart as $productCart)
                <li style="margin-top: 10px;box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;">
                    <div style="display: flex;align-items: center; width: 85%;">
                        <div class="left">
                            <img width="100" height="100" src="{{asset($productCart->options->image)}}" alt="">
                        </div>
                        <div class="right">
                            <span class="cd-qty">{{$productCart->qty}}x</span><span style="font-weight: 600">{{$productCart->name}}</span>
                            <div class="cd-price">{{number_format($productCart->price,0,",",",")}} Đ</div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="cd-cart-total">
                <p>Tổng đơn hàng: <span>{{\Gloudemans\Shoppingcart\Facades\Cart::subtotal()}}Đ</span></p>
            </div>
        </div>
    </div>

</div>
@endsection

