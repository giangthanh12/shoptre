
@extends("frontend_clone.layouts.master")
@section("content")
<style>


* {
	box-cart-sizing: border-box-cart;
}
/* css input increase decrease */
form {
    margin: 0 auto;
}

.value-button {
    display: inline-block;
    border: 1px solid #ddd;
    margin: 0px;
    width: 40px;
    height: 20px;
    text-align: center;
    vertical-align: middle;
    padding: 11px 0;
    background: #eee;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

.value-button:hover {
    cursor: pointer;
}

form #decrease {
    margin-right: -4px;
    border-radius: 8px 0 0 8px;
}

form #increase {
    margin-left: -4px;
    border-radius: 0 8px 8px 0;
}

form #input-wrap {
    margin: 0px;
    padding: 0px;
}

input.number {
    text-align: center;
    border: none;
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    margin: 0px;
    width: 40px;
    height: 40px;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
/* end input increase decrease */

.wrapper {
	max-width: 1200px;
	margin: 0 auto;
}
.wrapper h2 {
	text-transform: uppercase;
}
.project {
	display: flex;
}
.shop {
	flex: 75%;
}
.box-cart {
	display: flex;
	width: 100%;
    height: 150px;
	overflow: hidden;
	margin-bottom: 20px;
	background: #fff;
	transition: all .6s ease;
	box-cart-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
}
.box-cart:hover {
	border: none;
	transform: scale(1.01);
}
.box-cart img {
    width: 150px;
    height: 150px;
    object-fit: cover;
}
.content {
	padding: 0px 20px;
	width: 100%;
	position: relative;
    /*height: 80vh;*/
}
.content h3 {
	/*margin-bottom: 30px;*/
    margin-top: 0px;
}
.content h4 {
    margin-top:0px;
	/*margin-bottom: 50px;*/
}
.btn-area {
	position: absolute;
	bottom: 20px;
	right: 20px;
	padding: 10px 25px;
	background-color: #3a71a9;
	color: white;
	cursor: pointer;
	border-radius: 5px;
}
.btn-area:hover {
	background-color: #fff;
	color: #3a71a9;
    border: 1px solid #3a71a9;
	font-weight: 600;
}
.unit input {
	width: 40px;
	padding: 5px;
	text-align: center;
}
.btn-area i {
	margin-right: 5px;
}
.right-bar-cart {
	flex: 25%;
	margin-left: 20px;
	padding: 20px;
	height: 400px;
	border-radius: 5px;
	background: #fff;
	box-cart-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
}
.right-bar-cart hr {
	margin-bottom: 25px;
}
.right-bar-cart p {
	display: flex;
	justify-content: space-between;
	margin-bottom: 30px;
	font-size: 20px;
}
.right-bar-cart a {
	background-color: #00adef;
	color: #fff;
	text-decoration: none;
	display: block;
	text-align: center;
	height: 40px;
	line-height: 40px;
	font-weight: 900;
}
.right-bar-cart i {
	margin-right: 15px;
}
.right-bar-cart a:hover {
	background-color: #3972a7;
}
@media screen and (max-width: 700px) {
	.content h3 {
		margin-bottom: 15px;
	}
	.content h4 {
		margin-bottom: 20px;
	}
	.btn2 {
		display: none;
	}
	.box-cart {
		height: 150px;
	}
	.box-cart img {
		height: 150px;
		width: 200px;
	}
}
@media screen and (max-width: 900px) {
	.project {
		flex-direction: column;
	}
	.right-bar-cart {
		margin-left: 0;
		margin-bottom: 20px;
	}
}
@media screen and (max-width: 1250px) {
	.wrapper {
		max-width: 95%;
	}
}

</style>
<div class="wrapper">
    <h2 style="color: #00adef">Giỏ hàng của bạn</h2>
    <div class="project">
        <div class="shop">
            @forelse($dataCart as $productcart)
            <div class="box-cart" id="item-data-cart{{$productcart->id}}">
                <img src="{{asset($productcart->options->image)}}">
                <div class="content">
                    <h3>{{$productcart->name}}</h3>
                    <h4>Giá sản phẩm: {{number_format($productcart->price,0,",",",")}} Đ</h4>
                    <form>
                        <div class="value-button" id="decrease" onclick="decreaseValue({{$productcart->id}}, '{{$productcart->rowId}}')" value="Decrease Value">-</div>
                        <input type="number" value="{{$productcart->qty}}" class="number" id="number{{$productcart->id}}" value="0" />
                        <div class="value-button" id="increase" onclick="increaseValue({{$productcart->id}}, '{{$productcart->rowId}}')" value="Increase Value">+</div>
                    </form>
                    <p onclick="delProductCart('{{$productcart->rowId}}', {{$productcart->id}})" class="btn-area"><i aria-hidden="true" class="fa fa-trash"></i> <span class="btn2">Xóa</span></p>
                </div>
            </div>
            @empty
                <p>Giỏ hàng của bạn hiện tại đang rỗng, tiếp tục mua hàng <a href="{{route('client.products')}}">tại đây</a></p>
            @endforelse
        </div>
        <div class="right-bar-cart">
            <p><span><b>Tổng giá trị đơn hàng</b>: </span> <span id="totalValueOrder"> {{\Gloudemans\Shoppingcart\Facades\Cart::subtotal()}}Đ</span></p>
            <hr>
            <p><span><b>Phí vận chuyển:</b></span> <span>0đ</span></p>
            <hr>
            <p><span><b>Tổng:</b></span> <span id="totalCart">{{\Gloudemans\Shoppingcart\Facades\Cart::subtotal()}}Đ</span></p><a href="{{route("client.checkout")}}"><i class="fa fa-shopping-cart"></i>Thanh toán</a>
        </div>
    </div>
</div>

<script>
    function increaseValue(productId, rowId) {
        var qty = parseInt(document.getElementById('number'+productId).value, 10);
        qty += 1;
        $.ajax({
            type: "POST",
            data:{productId,rowId,qty},
            url: '/update-to-cart',
            dataType: "JSON",
            success: function (data) {
                if(data.result) {
                    showToast("success",data.message);
                    document.getElementById('number'+productId).value = qty;
                    getTotalCart();
                    showSmallCart();
                }
                else {
                    showToast("error",data.message);
                }
            },
            error: function(data) {
                showToast("error",data.message);
            },
        });
    }
    function getTotalCart() {
        $.ajax({
            type: "GET",
            data:{},
            url: '/get-total-cart',
            dataType: "JSON",
            success: function (data) {
                if(data.result) {
                    $("#totalValueOrder").text(data.data+"Đ");
                    $("#totalCart").text(data.data+"Đ");
                }
            },
            error: function(data) {
                showToast("error",data.message);
            },
        });
    }
    function decreaseValue(productId, rowId) {
        var qty = parseInt(document.getElementById('number'+productId).value, 10);
            qty -= 1;
            if(qty == 0) return;
        $.ajax({
            type: "POST",
            data:{productId,rowId,qty},
            url: '/update-to-cart',
            dataType: "JSON",
            success: function (data) {
                if(data.result) {
                    showToast("success",data.message);
                    document.getElementById('number'+productId).value = qty;
                    getTotalCart();
                    showSmallCart();
                }
                else {
                    showToast("error",data.message);
                }
            },
            error: function(data) {
                showToast("error",data.message);
            },
        });
    }

</script>
@endsection

