@extends("frontend_clone.layouts.master")
@section("content")
    <style>
        .price-show {
            display: flex;
            justify-content: space-around;
            padding-top: 13px;
        }
        .price-orderBy.active {
            background: hsl(180deg 51% 52%);
        }
    </style>
    <div class="content__link w-full">
       <div class="content__link__box container pad-1-lr">
          <div class="link__box__inside dis_flex text-fs-1 ">
             <a class="text__link" href="{{route("client.index")}}">Trang chủ</a>
             <i class="icon__text__link fa-solid fa-angle-right"></i>
             <a class="text__link" href="{{route("client.products")}}">Sản phẩm</a>
          </div>
       </div>
    </div>
    <div class="content__body container pad-1-lr">
       <div class="content__body__colum grid grid-clos-12">
          <div class="content__body__b1 col-span-3">
             <div class="body__b1 w-full">
                <!-- Box 1 -->
                <div class="b1__box1">
                   <p class="box__header text-tran">Mức giá</p>
                   <p class="box1__text">Giá tối thiểu</p>
                    <form action="{{$currentRoute}}" method="get">
                        {{-- check param có sẵn --}}

                        <input type="hidden" name="price" value="{{request()->get("price")}}"/>
                        <input type="hidden" name="category" value="{{request()->get("category")}}"/>
                        <div class="box1__text2__input">
                            <div class="text2__input pos-rela">
                                <input type="number" min="100000" name="min-price" value="{{old('min-price', request()->get('min-price'))}}" class="text2__input__box w-full">
                                <div class="text__input__inside pos-ab dis_flex">
                                    <p class="inside__text text-tran">VND</p>
                                </div>
                            </div>
                        </div>
                        <p class="box1__text">Giá tối đa</p>
                        <div class="box1__text3__input pos-rela">
                            <input type="number" min="100000" name="max-price" value="{{old('min-price', request()->get('max-price'))}}" class="text2__input__box w-full">
                            <div class="text__input__inside pos-ab dis_flex">
                                <p class="inside__text text-tran">VND</p>
                            </div>
                        </div>
                        <div class="box1__bnt w-full dis_flex ">
                            <button class="box1__bnt__inside text-nm pd-l-r-2 pd-tb-625">ÁP DỤNG</button>
                        </div>
                    </form>
                </div>
                <!-- Box 3 -->
                <div class="b1__box3">
                   <div class="b1__box2">
                      <p class="box__header text-tran">loại sản phẩm</p>

                       @forelse($categories as $category)
                           <label class="box2__checkmark dis_flex text-nm algin_center pos-rela">
                               <input class="checkmark checkbox-category" {{!empty($idCategoryChecked) && in_array($category->id,$idCategoryChecked) ? "checked" : ""}} value="{{$category->id}}"  type="checkbox">
                               <div class="box2__checkmark__text w-full dis_flex text-nm">
                                   <p class="mg-0">{{$category->category_name_vn}}</p>
                                   <p class="opacity-3 mg-0">{{$category->products_count}}</p>
                               </div>
                               <span ></span>
                           </label>
                       @empty
                       @endforelse
                   </div>
                </div>
             </div>
          </div>
          <div class="content__body__b2 col-span-9">
             <div class="body__b2">
                <div class="body__b2__header pd-tb-1">
                   <p class="body__b2__header__text text-tran text-fs-2">Danh sách sản phẩm</p>
                </div>
                <div class="body__b2__nav pd-tb-625">
                   <div class="body__b2__nav__inside dis_flex algin_center pd-l-r-2">
                      <p class="nav__text text-nm">Sắp xếp theo</p>
                      <a class="nav__bnt text-tran pd-tb-625 btn-increase price-orderBy {{$priceSelected == "asc" ? "active" : ""}}" style="text-decoration: none;"  href="">Giá thấp đến cao</a>
                      <a class="nav__bnt text-tran pd-tb-625 btn-decrease price-orderBy {{$priceSelected == "desc" ? "active" : ""}}" style="text-decoration: none;" href="">Giá cao đến thấp</a>
                   </div>
                </div>
                <div class="body__b2__products grid grid-clos-4">
                   @forelse ($products as $product)
                   <a class="b2__products__all" href="{{route("client.products.detail",$product->product_slug_vn)}}" style="position:relative;">
                        <div class="b2__products">
                            <div class="b2__products__box dis_flex">
                                <div class="b2__products__box__img pos-rela">
                                    <img style="width:180px;" class="products__img" src="{{asset($product->mainImage[0]->url)}}" alt="">
                                </div>
                            </div>
                            <span onclick="addToCart(event,{{ $product->id }})"><i style="position: absolute; top: 9px; right: 0; color: #e07fb0;" class="icon__cart fa-solid fa-cart-shopping"></i></span>
                            <h4 class="products__text text-nm">{{$product->product_name_vn}}</h4>
                            @if(!is_null($product->discount_price) && $product->discount_price > 0)
                           <div class="price-show">
                               <p class="products__price" style="font-size:14px;text-decoration: line-through; color: #808080ba;">{{number_format($product->selling_price,0,",",",")}} Đ</p>
                               <p class="products__price" style="font-size:14px;">{{number_format($product->discount_price,0,",",",")}} Đ</p>
                           </div>
                            @else
                                <p class="products__price" style="padding-top: 15px;" >{{number_format($product->selling_price,0,",",",")}} Đ</p>
                            @endif
                        </div>
                    </a>
                   @empty
                    <h3>Rỗng</h3>
                   @endforelse
                </div>

                 {{ $products->links("paginate.custom") }}
          </div>
       </div>
    </div>
        <script>

            $(document).ready(function() {
                //update parameter when change filter
                function updateParameter(urlCurrent, key, value) {
                    let url = new URL(urlCurrent);
                    let search_params = url.searchParams;
                    // new value of "key" is set to "value"
                    search_params.set(key, value);
                    search_params.set('page', 1);
                    // change the search property of the main url
                    url.search = search_params.toString();
                    return url.toString();
                }
                $('.btn-increase').click(function(e) {
                    e.preventDefault();
                    window.location.href = updateParameter(window.location.href, 'price', "asc");
                });
                $('.btn-decrease').click(function(e) {
                    e.preventDefault();
                    window.location.href = updateParameter(window.location.href, 'price', "desc");
                });
                $('.checkbox-category').click(function(e) {
                    let idCategory = [];
                    $( '.checkbox-category' ).each( function( index, checkbox ) {
                        if(checkbox.checked) {
                            idCategory.push($(this).val());
                        }
                    });
                    window.location.href = updateParameter(window.location.href, 'category', JSON.stringify(idCategory));
                });
            })
        </script>
@endsection
