@extends("frontend_clone.layouts.master")
<style>
    .price-show {
        display: flex;
        justify-content: space-around;
        padding-top: 13px;
    }
    .price-orderBy.active {
        background: hsl(180deg 51% 52%);
    }
</style>
@section("content")
    <style>
        ul{
            list-style: none outside none;
            padding-left: 0;
            margin: 0;
        }
        .table__body__link.border-left-none {
                border-left: none !important;
        }
    </style>
<div class="container ">
    @include("frontend_clone.body.slide")
    @include("frontend_clone.body.banner")
 </div>
 <!-- Best sell -->

 <div class="best-sell__box container pad-1-lr" style="margin-top: 100px;">
     <div class="best-sell__box__header">
         <img class="box__header__img" style="width: auto;" src="{{asset("frontend_clone/img/best-sell.png")}}" alt="">
     </div>
        <ul id="content-slider" class="content-slider">
            @forelse($productsHot as $productHot)
                <li>
                    <a class="table__body__link border-left-none" href="{{route("client.products.detail",$productHot->product_slug_vn)}}">
                        <div class="table__body__box">
                            <div class="body__box__img dis_flex">
                                <div class="img__inside pos-rela">
                                    <img class="table__img" width="180" height="180" src="{{asset($productHot->mainImage[0]->url)}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ">
                                </div>
                            </div>
                            <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao </h4>
                            @if(!is_null($productHot->discount_price) && $productHot->discount_price > 0)
                                <div class="price-show">
                                    <p class="products__price" style="font-size:14px;text-decoration: line-through; color: #808080ba;">{{number_format($productHot->selling_price,0,",",",")}} Đ</p>
                                    <p class="products__price" style="font-size:14px;">{{number_format($productHot->discount_price,0,",",",")}} Đ</p>
                                </div>
                            @else
                                <p class="products__price" style="padding-top: 15px; font-size:14px;" >{{number_format($productHot->selling_price,0,",",",")}} Đ</p>
                            @endif
                        </div>
                    </a>
                </li>
            @empty
                <h4>Rỗng</h4>
            @endforelse

        </ul>
    </div>
 <!-- Featured Products -->
 <!-- New Products -->
 <div class="new-products container pad-1-lr">
    <div class="new-products__header container pad-1-lr">
       <div class="new-products__header__img w-full dis_flex">
          <img class="header__img" src="{{asset("frontend_clone/img/logo_down.png")}}" alt="">
       </div>
    </div>
    <div class="new-products__table">
       <div class="new-products__table__header text-tran">
          <h3 style="margin: 0;">Sản phẩm mới nhất</h3>
       </div>
       <div class="new-products__table__body grid-clos-6">
           @forelse($productsLatest as $productLatest)
               <a class="table__body__link" href="{{route("client.products.detail",$productLatest->product_slug_vn)}}" style="position:relative;">
                   <div class="table__body__box">
                       <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                               <img class="table__img" width="180" height="180" src="{{asset($productLatest->mainImage[0]->url)}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ">
                           </div>
                       </div>
                       <span onclick="addToCart(event,{{ $productLatest->id }})"><i style="position: absolute; top: 9px; right: 0; color: #e07fb0;" class="icon__cart fa-solid fa-cart-shopping"></i></span>
                       <h4 class="body__box__text text-nm">{{$productLatest->product_name_vn}}</h4>
                       @if(!is_null($productLatest->discount_price) && $productLatest->discount_price > 0)
                           <div class="price-show">
                               <p class="products__price" style="font-size:14px;text-decoration: line-through; color: #808080ba;">{{number_format($productLatest->selling_price,0,",",",")}} Đ</p>
                               <p class="products__price" style="font-size:14px;">{{number_format($productLatest->discount_price,0,",",",")}} Đ</p>
                           </div>
                       @else
                           <p class="products__price" style="padding-top: 15px; font-size:14px;" >{{number_format($productLatest->selling_price,0,",",",")}} Đ</p>
                       @endif
                   </div>
               </a>
           @empty
               <h4>Rỗng</h4>
           @endforelse
       </div>
       <a class="new-products__table__btn__link" href="{{route('client.products')}}">
          <div class="new-products__table__btn dis_flex">
             <button style="margin-bottom: 20px;" class="btn text-tran">
                <span>Xem tất cả sản phẩm</span>
                <i class="btn__icon fa-solid fa-right-long"></i>
             </button>
          </div>
       </a>
    </div>
 </div>
<script>
    $(document).ready(function() {
        $("#content-slider").lightSlider({
            auto:true,
            item:6,
            loop:true,
            keyPress:true
        });
    })
</script>
@endsection
