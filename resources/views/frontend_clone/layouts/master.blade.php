<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;400&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>


    <!-- Customizable CSS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">


    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="{{asset('frontend_clone/css/product_detail.css')}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/main.css")}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/style.css")}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/media.css")}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/products.css")}}">
    <link rel="stylesheet" href="{{asset('frontend_clone/css/lightslider.css')}}">
    <script src="{{asset("frontend_clone/js/btnSigIn.js")}}"></script>
    <meta name="viewport" content="width=device-width, inital-scale=1.0">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/validate.css")}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/font/fontawesome-free-6.1.2-web/css/all.min.css")}}" >
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="{{asset("frontend_clone/js/jquery.validate.min.js")}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    @livewireStyles

{{--    <link rel="stylesheet" href="assets/css/bootstrap.min.css">--}}
{{--    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
{{--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800&display=swap" rel="stylesheet">--}}
{{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">--}}
{{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">--}}
{{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">--}}
@stack("css")
    <title>HiBeYo</title>
</head>
<body>
   <div class="all">

      <!-- Header -->
    @include("frontend_clone.body.header")
      <div class="content">
            @yield("content")
      </div>
      <!-- FOOTER -->
      @include("frontend_clone.body.footer")

       <div id="cd-cart">
            {{--  ajax mini cart   --}}
       </div> <!-- cd-cart -->
   </div>

   @include("frontend_clone.body.authenticate")
</body>
<script src="{{asset('frontend_clone/js/lightslider.js')}}"></script>
<script src="{{asset('frontend_clone/js/product_detail.js')}}"></script>
<script src="{{asset('frontend_clone/js/modernizr.js')}}"></script>
<script src="{{asset('frontend_clone/js/main.js')}}"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@livewireScripts


<script>
    const URL_ROOT_IMAGE = "{{asset("/")}}";
    @if (Session::has('message'))
    var message = "{{Session::get('message')}}";
    var type = "{{Session::get('type')}}";
    console.log(message);
    switch(type) {
        case 'success':
        toastr.success(message);
        break;
        case 'info':
        toastr.info(message);
        break;
        case 'error':
        toastr.error(message);
        break;
        case 'warning':
        toastr.warning(message);
        break;
    }
    @endif
@if(Session::has("errors"))
    var objectErrors = <?php echo Session::get('errors') ?>;
    var htmlErrors = `<ul>`;
    Object.entries(objectErrors).forEach(([key, value]) => {
        htmlErrors += `<li>${value}</li>`;
    });
    htmlErrors+= `</ul>`;
    toastr.error(htmlErrors);
@endif
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function showToast(type, message) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: type,
            title: message

        })
    }
    function addToCart(event, productId,qty=1) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            data:{productId, qty},
            url: '/add-to-cart',
            dataType: "JSON",
            success: function (data) {
                if(data.result) {
                    showSmallCart();
                    showToast("success",data.message);
                }
                else {
                    showToast("error",data.message);
                }
            },
            error: function(data) {
                alert(data.message);
            },
        });
    }
    function showSmallCart() {
        $.ajax({
            type: "get",
            data:{},
            url: '/get-small-cart',
            dataType: "JSON",
            success: function (data) {
                if(data.result) {
                    if(data.data.countCart == 0) {
                        $(".count-data-cart").empty();
                        let html = `<h2>Giỏ hàng của bạn rỗng</h2>`;
                        $("#cd-cart").html(html);
                    } else {
                        $(".count-data-cart").text(data.data.countCart);
                        let html = `<h2>Giỏ hàng của bạn</h2>`;
                            html += `<ul class="cd-cart-items">`;
                        $.each( data.data.dataCart, function( key, value ) {
                            html += `<li>
                                        <div style="display: flex;align-items: center; width: 85%;">
                                            <div class="left">
                                                <img width="100" height="100" src="${URL_ROOT_IMAGE+value.options.image}" alt="">
                                            </div>
                                            <div class="right">
                                                <span class="cd-qty">${value.qty}x</span>${value.name}
                                                <div class="cd-price">${new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.price)}</div>
                                            </div>
                                        </div>
                                        <a href="#0" style="text-align: center;
                                                        background: red;
                                                        color: #fff;
                                                        display: flex;
                                                        justify-content: center;
                                                        align-items: center;" onclick="delProductCart('${value.rowId}',${value.id})"  class="cd-item-remove cd-img-replace"><i class="fa-solid fa-trash"></i></a>
                                    </li>`;
                        });

                        html += `</ul>
                        <div class="cd-cart-total">
                            <p>Tổng đơn hàng: <span>${data.data.totalCart} ₫</span></p>
                        </div> <!-- cd-cart-total -->
                        <a href="/checkout" style="text-decoration: none;" class="checkout-btn">Thanh toán</a>
                        <a class="cd-go-cart" style="text-decoration: none;" href="{{route("client.carts")}}">Đi tới giỏ hàng</a>`;
                        $("#cd-cart").html(html);
                    }
                }
            },
            error: function(data) {
                alert(data.message);
            },
        });
    }
    function delProductCart(rowId, productId) {
        $.ajax({
            type: "POST",
            data:{rowId},
            url: '/delete-to-cart',
            dataType: "JSON",
            success: function (data) {
                if(data.result) {
                    showToast("success",data.message);
                    $("#item-data-cart"+productId).remove();
                    if($('#totalValueOrder').length > 0) {
                        getTotalCart();
                    }
                    showSmallCart();
                }
                else {
                    showToast("error",data.message);
                }
            },
            error: function(data) {
                showToast("error",data.message);
            },
        });
    }
    showSmallCart();
</script>


</html>
