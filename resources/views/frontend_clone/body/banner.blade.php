<div class="banner container ">
    <div class="banner__img dis_flex" style="padding-left: 0px;padding-right: 0px;">
       <div class="img__banner">
          <img class="img__banner__inside" src="{{asset('frontend_clone/img/1.png')}}" alt="">
       </div>
       <div class="img__banner">
          <img class="img__banner__inside" src="{{asset('frontend_clone/img/2.png')}}" alt="">
       </div>
       <div class="img__banner">
          <img class="img__banner__inside" src="{{asset('frontend_clone/img/3.png')}}" alt="">
       </div>
    </div>
    <div class="banner__img-longer container w-full">
       <div  class="img-longer" style="display: flex; justify-content: center;">
          <img class="img__longer loca-1" src="{{asset('frontend_clone/img/bnn1.png')}}" alt="banner img">
          <img class="img__longer loca-2" src="{{asset('frontend_clone/img/bnn2.png')}}" alt="banner img">
          <img class="img__longer loca-3" src="{{asset('frontend_clone/img/bnn3.png')}}" alt="banner img">
          <img class="img__longer loca-4" src="{{asset('frontend_clone/img/bnn4.png')}}" alt="banner img">
       </div>
    </div>
 </div>
