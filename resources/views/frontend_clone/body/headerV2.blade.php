{{-- @php
    $categories = \App\Models\Category::withCount(['products'=> function($q){
       $q->where('status', ACTIVE);
}])->get();
@endphp --}}
<style>
    .list-products-search {
        position: absolute;
        z-index: 1;
        background: white;
        width: 100%;
        top: 45px;
        border-radius: 12px;
    }
    .item-product-search:first-child {
        margin-top: 8px;
    }
    .item-product-search {
        display: flex;
        gap: 16px;
        text-decoration: none;
    }
    .li-item-product-search {
        margin-bottom: 8px;
        border-bottom: 1px solid #ded6d6;
        padding-bottom: 8px;
    }
    .li-item-product-search.active {
        background: greenyellow;
    }
</style>
<div class="header">
    <div class="header__up">
       <div class="header__up--all container dis_flex">
          <div class="header__up__menu-390 ">
             <div class="menu-390__bnt dis_flex">
                <button type="button" class="menu__bnt">
                   <i class="icon__menu__bnt fa-solid fa-list"></i>
                </button>
                <div class="header__up__menu-390__list pos-ab w-full">
                   <div class="menu-390__list">
                      <ul class="menu-390__list__item pad-1-lr show-menu">
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="{{route("client.index")}}">Trang chủ</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">Tin tức</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">Giới thiệu</a>
                         </li>
{{--                         <li class="menu-390__item h-full text-tran">--}}
{{--                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">khuyến mãi</a>--}}
{{--                         </li>--}}
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="{{route("client.products")}}">sản phẩm</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">liên hệ</a>
                         </li>
                      </ul>
                   </div>
                </div>
             </div>
          </div>
          <!-- Logo -->
          <div class="header__up__logo dis_flex">
             <div class="logo__img">
                <a href="" class="logo__img--outside">
                   <img src="{{asset("frontend_clone/img/logo2 1.png")}}" alt="logo" class="logo__img--inside">
                </a>
                <img src="{{asset("frontend_clone/img/Frame.png")}}" alt="logo down" class="logo__img__dow">
             </div>
          </div>
          <!-- Search-bar -->
          <div class="header__up__search-bar">
             @livewire("product-search")
          </div>

          <div class="header__up__user dis_flex algin_center">
             <div class="user__log-sig">
                <i class="fa-solid fa-user"></i>
                @if (auth()->guard('web')->check())
                    <span style="color:#fff; font-size:0.86rem;">Xin chào, {{auth()->user()->name}} (<a href="{{route("user.logout")}}" style="color:#fff">Logout</a>)</span>
                @else
                    <button id="login__user" class="user__log-in box text" onclick="myFunction()">Đăng nhập</button>
                    <button id="sigin__user" class="user__sig-in box text" onclick="mySigin()">Đăng ký</button>
                @endif
             </div>
             <!-- User -->
             <div class="user__cart dis_flex algin_center" id="cd-cart-trigger">
                <a class="cd-img-replace" href="#0">
                    <i class="icon__cart fa-solid fa-cart-shopping"></i>
                    <p class="cart__text text">Giỏ hàng</p></a>
             </div>
          </div>
       </div>
    </div>

    <div class="header__dow">
      <div class="header__dow__nav-bar container ">
          <div class="nav-bar">
             <ul class="nav-bar__list dis_flex">
                <li class="list text text-tran grid__column-4 menu__btn"  data-toggle="dropdown">
                   <button class="menu__box" onclick="dropDown()">
                      <div class="menu__box__btn">
                         <i class="fa-solid fa-bars icon__btn"></i>
                         <span class="menu__text text-tran text">danh mục sản phẩm </span>
                      </div>
                   </button>
                   <div class="list__menu__box">
                      <ul class="slide__menu__list w-full">
                          {{-- @forelse($categories as $category)
                         <li class="menu-list pad-1 text-tran border-col-1 text-nm"><a style="text-decoration: none; color:#3D3D3D" href="{{route('client.category', $category->category_slug_vn)}}">{{$category->category_name_vn}}</a></li>
                          @empty
                          @endforelse --}}
                      </ul>
                   </div>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="{{route("client.index")}}">Trang chủ</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="">Tin tức</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="">Giới thiệu</a>
                </li>
{{--                <li class="list text text-tran">--}}
{{--                   <a class="list__link" href="">khuyến mãi</a>--}}
{{--                </li>--}}
                <li class="list text text-tran">
                   <a class="list__link" href="{{route("client.products")}}">sản phẩm</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link border__end" href="">liên hệ</a>
                </li>
             </ul>
          </div>
      </div>
    </div>
 </div>
