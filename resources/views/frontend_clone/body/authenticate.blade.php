  <div class="modal dis_flex" id="modal__box" >
      <div class="modal__overlay" id="modal__overlay__box" onclick="myModal()">

      </div>
      <div class="modal__body">
         <!-- Sig in -->
          <div class="auth-form tabcontent" id="Sigin" style="display: none;">
                <div class="auth-form__container">
                <div class="auth-form__header dis_flex">
                    <h3 class="auth-form__heading">Đăng ký</h3>
                    <button class="auth-form__btn tablinks" onclick="openCity(event, 'Login')"s>Đăng nhập</button>
                </div>
                <form id="form-register" action="{{ route('register') }}" method="POST">
                @csrf
                <div class="auth-form__form">
                    <div class="auth-form__gr">
                        <input type="text" id="name" name="name" class="auth-form__input" placeholder="Tên của bạn">
                    </div>
                    <div class="auth-form__gr">
                        <input type="text" id="email" name="email" class="auth-form__input" placeholder="Email của bạn">
                    </div>
                    <div class="auth-form__gr">
                        <input type="password" name="password" id="password" class="auth-form__input" placeholder="Nhập lại mật khẩu">
                    </div>
                    <div class="auth-form__gr">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="auth-form__input" placeholder="Nhập lại mật khẩu">
                    </div>
                </div>
                <div class="auth-form__control">
                    <button type="button" class="bnt bnt-back bnt--back" onclick="btnBack()">TRỞ LẠI</button>
                    <button type="submit" class="bnt bnt--primary">ĐĂNG KÝ</button>
                </div>
            </form>
                </div>

         </div>

         <!-- Log in -->
         <div class="auth-form tabcontent" id="Login" style="display: none;">
            <div class="auth-form__container">
                <form method="POST" id="form-login" action="{{route('login')}}">
                    @csrf
                    <div class="auth-form__header dis_flex">
                        <h3 class="auth-form__heading">Đăng nhập</h3>
                        <button class="auth-form__btn tablinks" type="button" onclick="openCity(event, 'Sigin')" >Đăng ký</button>
                     </div>

                     <div class="auth-form__form">
                        <div class="auth-form__gr">
                           <input type="email" name="email" id="email" class="auth-form__input" placeholder="Email của bạn">
                        </div>
                        <div class="auth-form__gr">
                           <input type="password" name="password" id="password" class="auth-form__input" placeholder="Mật khẩu của bạn">
                        </div>
                     </div>

                     <div class="auth-form__aside">
                        <div class="auth-form__help">
                           <a href="{{route("password.request")}}" class="auth-form__help-link auth-form__help-ps">Quên mật khẩu</a>
                           <span class="auth-form__help-space"></span>
                        </div>
                     </div>
                     <div class="auth-form__control">
                        <button type="button" class="bnt bnt-back bnt--back" onclick="btnBack()">TRỞ LẠI</button>
                        <button type="submit" class="bnt bnt--primary">ĐĂNG NHẬP</button>
                     </div>
                </form>
            </div>
         </div>
      </div>
   </div>
   <style>
        label.error {
        color: #eb3434;
        }
    </style>
<script type="text/javascript">
    $("#form-login").validate({
        rules: {
            email: {
            required: true,
            email: true
            },
            password:{
                required: true,
                minlength:6,
            },
        },
        messages: {
            password: {
                required: 'Vui lòng điền mật khẩu của bạn',
                minlength: 'Mật khẩu yêu cầu tối thiểu 6 ký tự'
            },
            email: {
            required: "Vui lòng nhập email của bạn",
            email: "Email của bạn không đúng định dạng"
            }
        }
        });
        $("#form-register").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
            },
            phone: {
                required:true,
                number:true,
                minlength:true,
            },
            password:{
                required: true,
                minlength:6,
            },
            password_confirmation:{
                equalTo: "#password"

            },
        },
        messages: {
            password: {
                required: 'Vui lòng điền mật khẩu của bạn',
                minlength: 'Mật khẩu yêu cầu tối thiểu 6 ký tự'
            },
            password_confirmation:{
                equalTo: 'Mật khẩu của bạn không khớp',

            },
            email: {
                required: "Vui lòng nhập email của bạn",
                email: "Email của bạn không đúng định dạng"
            },
            name: {
                required: 'Vui lòng nhập tên của bạn',
            },
            phone: {
                required:'Vui lòng nhập điện thoại của bạn',
                number:'Yêu cầu bắt buộc số từ 0-9',
                minlength:'Tối thiểu 10 số',
            }
        }
        });
</script>
