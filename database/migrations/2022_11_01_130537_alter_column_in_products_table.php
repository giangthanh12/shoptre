<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnInProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
                $table->integer("brand_id")->nullable(true)->change();
                $table->integer("subCategory_id")->nullable(true)->change();
                $table->integer("subsubCategory_id")->nullable(true)->change();
                $table->string("product_name_en", 255)->nullable(true)->change();
                $table->string("product_slug_en", 255)->nullable(true)->change();
                $table->string("product_tags_vn", 255)->nullable(true)->change();
                $table->string("product_tags_en",255)->nullable(true)->change();
                $table->string("product_name_en", 255)->nullable(true)->change();
                $table->string("product_size_vn", 255)->nullable(true)->change();
                $table->string("product_size_en", 255)->nullable(true)->change();
                $table->string("product_color_vn", 255)->nullable(true)->change();
                $table->string("product_color_en", 255)->nullable(true)->change();
                $table->string("short_descp_en", 255)->nullable(true)->change();
                $table->text("long_descp_en")->nullable(true)->change();
                $table->string("product_thumbnail",255)->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
