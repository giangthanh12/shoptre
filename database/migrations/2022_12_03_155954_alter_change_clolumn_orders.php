<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterChangeClolumnOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger("matp")->nullable()->change();
            $table->unsignedBigInteger("maqh")->nullable()->change();
            $table->unsignedBigInteger("maxa")->nullable()->change();
            $table->string("order_number")->nullable()->change();
            $table->string("invoice_no")->nullable()->change();
            $table->string("order_date")->nullable()->change();
            $table->string("order_month")->nullable()->change();
            $table->string("order_year")->nullable()->change();
            $table->string("currency")->nullable()->change();
            $table->renameColumn('notes', 'address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
