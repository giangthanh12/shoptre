<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\ProductController;
use App\Http\Controllers\Client\CartController;
use App\Http\Controllers\Client\CategoryController;
use App\Models\Item;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('client.')->group(function () {
    Route::get('/', [HomeController::class, "index"])->name("index");
    Route::get('/products',[ProductController::class, "index"])->name("products");
    Route::get('/product/{slug}',[ProductController::class, "detail"])->name("products.detail");
    Route::get('/cart',[CartController::class, "index"])->name("carts");
    Route::post('/add-to-cart',[CartController::class, "addToCart"])->name("cart.add");
    Route::post('/update-to-cart',[CartController::class, "updateToCart"])->name("cart.update");
    Route::post('/delete-to-cart',[CartController::class, "deleteToCart"])->name("cart.delete");
    Route::get('/get-total-cart',[CartController::class, "getTotalCart"])->name("cart.get.total.cart");
    Route::get('/get-small-cart',[CartController::class, "getSmallCart"])->name("cart.get.small.cart");
    Route::get('/category/{slug}', [CategoryController::class, 'index'])->name('category');
    Route::any("checkout", [CartController::class, "checkout"])->name("checkout");
    Route::any("callback-momo", [CartController::class, "callback"])->name("callback.momo");
    Route::get('/about', [HomeController::class, 'about'])->name('about');
    Route::post('/contact', [HomeController::class, 'contact'])->name('contact');
    Route::get('/detailV2', [CategoryController::class, function(){
        return view('frontend_clone.frontend.detailV2');
    }]);
});



Route::get("/test", function() {
//    $check =  Item::with('parts')
//     ->whereHas('parts', function($query) {
//        $query->whereNotNull('name');
//     })
//     ->get();

$check = Item::whereHas('parts', function($query) {
         $query->where('name','part2');
    })
    ->get();
    return $check;
});

Auth::routes();

Route::get('user/logout', function() {
     auth()->guard("web")->logout();
     return back();
})->name('user.logout');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
