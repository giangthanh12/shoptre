<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\AdminController;

//use App\Http\Controllers\Admin\OrderController;
Route::middleware("admin.guest")->match(['get', 'post'], '/login', "Admin\AuthController@login")->name("login");
Route::match(['post'], '/logout', "Admin\AuthController@logout")->name("logout");
Route::middleware("admin.auth")->group(function() {
    Route::get("dashboard", "Admin\DashboardController@dashboard")->name("dashboard");
    Route::any("profile", "Admin\AuthController@profile")->name("profile");
    Route::any("changePassword", "Admin\AuthController@changePassAdmin")->name("changePass");
    // route Category Admin
    Route::group(['prefix' => 'category', 'as'=>'category.'], function() {
        Route::get('/', [CategoryController::class, 'viewCategory'])->name('view');
        Route::POST('store', [CategoryController::class, 'storeCategory'])->name('store');
        Route::GET('edit-category/{id}', [CategoryController::class, 'editCategory'])->name('edit');
        Route::POST('update/{id}', [CategoryController::class, 'updateCategory'])->name('update');
        Route::GET('delete/{id}', [CategoryController::class, 'deleteCategory'])->name('delete');
    });

    // route country Admin
    Route::group(['prefix' => 'country', 'as'=>'country.'], function() {
        Route::get('/', [CountryController::class, 'index'])->name('view');
        Route::POST('store', [CountryController::class, 'store'])->name('store');
        Route::GET('edit/{id}', [CountryController::class, 'edit'])->name('edit');
        Route::POST('update/{id}', [CountryController::class, 'update'])->name('update');
        Route::GET('delete/{id}', [CountryController::class, 'delete'])->name('delete');
    });

    //route slider
    Route::group(['prefix' => 'slider', 'as'=>'slider.'], function() {
        Route::get('/', [SliderController::class, 'manageSlider'])->name('manage');
        Route::POST('store', [SliderController::class, 'storeSlider'])->name('store');
        Route::GET('edit/{id}', [SliderController::class, 'editSlider'])->name('edit');
        Route::POST('update/{id}', [SliderController::class, 'updateSlider'])->name('update');
        Route::GET('delete/{id}', [SliderController::class, 'deleteSlider'])->name('delete');
        Route::get('inactive/{id}', [SliderController::class, 'inactiveSlider'])->name('inactive');
        Route::get('active/{id}', [SliderController::class, 'activeSlider'])->name('active');
    });

    // route product
    Route::group(['prefix' => 'product', 'as'=>'product.'], function() {
        Route::get('add', [ProductController::class, 'addProduct'])->name('add');
        Route::POST('save', [ProductController::class, 'saveProduct'])->name('save');
        Route::get('manage', [ProductController::class, 'manageProduct'])->name('manage');
        Route::get('edit/{id}', [ProductController::class, 'editProduct'])->name('edit');
        Route::POST('update', [ProductController::class, 'updateProduct'])->name('update');
        Route::POST('update/multiImage', [ProductController::class, 'updateMultiImage'])->name('updateMultiImage');
        Route::get('edit/{id}', [ProductController::class, 'editProduct'])->name('edit');
        Route::get('deleteMultiImage/{id}', [ProductController::class, 'deleteMultiImage'])->name('deleteMultiImage');
        Route::get('active/{id}', [ProductController::class, 'activeProduct'])->name('active');
        Route::get('inactive/{id}', [ProductController::class, 'inactiveProduct'])->name('inactive');
        Route::get('delete/{id}', [ProductController::class, 'deleteProduct'])->name('delete');
    });

    // route order admin
    Route::group(['prefix' => 'order', 'as'=>'order.'], function() {
        Route::get('pending', [OrderController::class, 'managePendingOrder'])->name('managePendingOrder');
        Route::get('detail/{order_id}', [OrderController::class, 'detailOrder'])->name('detail');
        Route::get('pending/processing/{order_id}', [OrderController::class, 'updateProcessingOrder']);
        Route::get('processing', [OrderController::class, 'manageProcessingOrder'])->name('manageProcessingOrder');
        Route::get('processing/shipped/{order_id}', [OrderController::class, 'updateShippedOrder']);
        Route::get('shipped', [OrderController::class, 'manageShippedOrder'])->name('manageShippedOrder');
        Route::get('shipped/delivered/{order_id}', [OrderController::class, 'updateDeliveredOrder']);
        Route::get('delivered', [OrderController::class, 'manageDeliveredOrder'])->name('manageDeliveredOrder');
        Route::get('delivered/return/{order_id}', [OrderController::class, 'updateReturnOrder']);
        Route::get('cancel', [OrderController::class, 'manageCancelOrder'])->name('manageCancelOrder');
        Route::get('update-cancel/{order_id}', [OrderController::class, 'updateCancelOrder'])->name('updateCancelOrder');
        Route::get('invoice/{order_id}', [OrderController::class, 'invoiceOrder'])->name('invoice');
        Route::get('return', [OrderController::class, 'manageReturnOrder'])->name('manageReturnOrder');
        Route::get('return/success/{id}', [OrderController::class, 'returnSuccess'])->name('returnSuccess');
    });
    // route order admin
    Route::group(['prefix' => 'contact', 'as'=>'contact.'], function() {
        Route::get('pending', [ContactController::class, 'index'])->name('view');
    });

    // route manage admin
    Route::group(['prefix' => 'adminRole','middleware'=>['CheckRole']], function() {
        Route::get('manage', [AdminController::class, 'manageAdmin'])->name('adminRole.manage');
        Route::get('add', [AdminController::class, 'addAdmin'])->name('adminRole.add');
        Route::post('store', [AdminController::class, 'storeAdmin'])->name('adminRole.store');
        Route::get('edit/{id}', [AdminController::class, 'editAdmin'])->name('adminRole.edit');
        Route::get('delete/{id}', [AdminController::class, 'deleteAdmin'])->name('adminRole.delete');
        Route::post('update/{id}', [AdminController::class, 'updateAdmin'])->name('adminRole.update');
        Route::post('delete/{id}', [AdminController::class, 'deleteAdmin'])->name('adminRole.delete');
    });
});


?>
